# Crontab - Notes


# Set a shell
`SHELL=/bin/bash`

# Crontab format
```bash
* * * * *  command_to_execute  
- - - - -
| | | | |
| | | | +- day of week (0 - 7) (where sunday is 0 and 7)
| | | +--- month (1 - 12)
| | +----- day (1 - 31)
| +------- hour (0 - 23)
+--------- minute (0 - 59)
```

## Example entries

### every 15 min
`*/15 * * * * /home/user/command.sh`  
### every midnight
`0 * * * * /home/user/command.sh`  
### every Saturday at 8:05 AM
`5 8 * * 6 /home/user/command.sh`

   
## Schedule
| Minute    |Hour   | Day of month  |Month          |Day of week|
|:---       | :---  | :----         | :----         | :---      |
|0          |1       |15            |1,3,5,7,9,11   |*|
|0-59/15    |*      |*              |*              |*|
|30         |*      |*              |*              |wed,fri|
|0,30       |0-5,17-23  |*          |*              |*|
|0          |0      |1              |1              |*|
|0          |0      |*              |*              |0|
|30         |0      |10,20,30       |*              |6|
   


## Shorthand for common schedules

|Shorthand      | Description   |
| :---          | :---          |
| @reboot       | Run the command whenever the machine reboots.|
| @daily        | A shorthand for once per day
| @weekly       | A shorthand for once per week.|
| @annually     | A shorthand for once per year. You can also write this as @yearly.|
| @midnight     | Run the command once per day at midnight. A synonym for this shorthand is @daily.|

Cron has specific strings you can use to create commands quicker:

- @hourly: Run once every hour i.e. “0 * * * *“
- @midnight: Run once every day i.e. “0 0 * * *“
- @daily: same as midnight
- @weekly: Run once every week, i.e. “0 0 * * 0“
- @monthly: Run once every month i.e. “0 0 1 * *“
- @annually: Run once every year i.e. “0 0 1 1 *“
- @yearly: same as @annually
- @reboot: Run once at every startup
