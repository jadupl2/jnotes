# at - Notes

## To schedule a one time task

```bash
at {time}
{command 0}
{command 1}
Ctrl-d
```

## {time} can be either

```bash
at now | midnight | noon | teatime (4pm) | HH:MM
at now + N {minutes | hours | days | weeks}
at MM/DD/YY
echo "$SADMIN/bin/autoupdate.sh >/dev/null 2>&1"| at now
```

## To list pending jobs

```bash
atq
```

## To remove a job (use id from atq)

```bash
atrm {id}
```
