# LDAP Notes


### Assign Admin Password

```bash
[root@gumby openldap]# slappasswd
New password: 
Re-enter new password: 
{SSHA}9w/togO9jecTTR4EzgPwGmFGH9S6aTJn
[root@gumby openldap]# 
```


### Edit client config file

```bash
vi /etc/openldap/ldap.conf
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.
include /etc/openldap/schema/core.schema
include /etc/openldap/schema/cosine.schema
include /etc/openldap/schema/nis.schema

#BASE   dc=example, dc=com
#URI    ldap://ldap.example.com ldap://ldap-master.example.com:666

#SIZELIMIT      12
#TIMELIMIT      15
#DEREF          never

TLSCipherSuite          HIGH
TLSCertificateFile      /etc/openldap/ssl/sldapd-cert.crt
TLSCertificateKeyFile   /etc/openldap/ssl/sldapd-key.pem

security                ssf=128
password-hash           {SSHA}

database                bdb
directory               /var/lib/ldap

suffix                  "dc=maison,dc=ca"
rootdn                  "cn=Manager,dc=maison,dc=ca" 
rootpw                  {SSHA}9w/togO9jecTTR4EzgPwGmFGH9S6aTJn

access to attrs=userPassword
        by self write
        by dn="uid=root,ou=People,dc=maison,dc=ca" write
        by * auth

access to * by * read
```

### Check if OpenSSL Is installed

```bash
[root@gumby MyNotes]# rpm -qa | grep -i openssl
pyOpenSSL-0.6-1.p24.7.2.2
openssl-0.9.8b-8.3.el5
openssl-devel-0.9.8b-8.3.el5
[root@gumby MyNotes]# 
```

### Creating Keys and Certificates

```bash
[root@gumby MyNotes]# openssl req -x509 -days 365 -newkey rsa: \
> -nodes -keyout slapd-key.pem \
> -out slapd-cert.crt
Generating a 1024 bit RSA private key
................................++++++
......................................................++++++
writing new private key to 'slapd-key.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [GB]:CA
State or Province Name (full name) [Berkshire]:Quebec
Locality Name (eg, city) [Newbury]:Laval
Organization Name (eg, company) [My Company Ltd]:Anemone Ltd
Organizational Unit Name (eg, section) []:IT
Common Name (eg, your name or your server's hostname) []:gumby.slac.ca
Email Address []:jadupl2@gmail.com
[root@gumby MyNotes]# 
```

```bash
[root@gumby MyNotes]# ls -l slapd*
-rw-r--r-- 1 root root 1289 Jul 29 11:43 slapd-cert.crt
-rw-r--r-- 1 root root  887 Jul 29 11:43 slapd-key.pem
[root@gumby MyNotes]# 

[root@gumby openldap]# mkdir /etc/openldap/ssl

[root@gumby MyNotes]# cp slapd-key.pem /etc/openldap/ssl
[root@gumby MyNotes]# cp slapd-cert.crt  /etc/openldap/ssl

[root@gumby MyNotes]# cd /etc/openldap/ssl
[root@gumby ssl]# chmod 600 *
[root@gumby ssl]# ls -l
total 3
-rw------- 1 root root 1289 Jul 29 11:52 slapd-cert.crt
-rw------- 1 root root  887 Jul 29 11:52 slapd-key.pem
[root@gumby ssl]# 
```

### Starting the LDAP Server

```bash
[root@gumby openldap]# service ldap start
Checking configuration files for slapd:  config file testing succeeded
                                                           [  OK  ]
Starting slapd:                                            [  OK  ]
[root@gumby openldap]# 
```


