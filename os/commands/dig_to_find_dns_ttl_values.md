# Using dig to Find DNS Time to Live (TTL) Values

The dig command is a handy DNS information and troubleshooting tool. It can be used to grab a host or domain’s TTL (time to live) values.
This information can be critical to planning a DNS cut over, and how long to leave the old server on.
In most circumstances I'd recommend running a low (5 minute or less) value for normal operation, but some ISPs and providers still are shy about decreasing these values so it is always a good idea to check.

### TTL for a Specific Host

Dig's default output provides the TTL information, it is the number proceeding the record type (underlined below):
````
# dig +nocmd +noall +answer www.ateamsystems.com
www.ateamsystems.com.   270     IN      A     69.55.231.82
````
Note: If your default DNS server is not the authoritative server for the zone you are digging dig will show the time remaining (until the next refresh) instead of the raw TTL value in this position.
You can work around this by directing dig to specifically use one of the domain's servers, for example I know that ns1.ateamservers.com is authoritative for this domain:
````
# dig +nocmd +noall +answer @ns1.ateamservers.com www.ateamsystems.com
www.ateamsystems.com. 300 IN A 69.55.231.82
````

### Default TTL (and negative-TTL) for a Domain

We can also pull the domain-wide TTL setting, which controls negative-TTLs (how long a server will cache an NX or 'nothing there' reply). This will also break out the SOA into an easier to read format:
````
# dig +nocmd +multiline +noall +answer any ateamsystems.com
ateamsystems.com.      300 IN SOA ns.ateamservers.com. dns.ateamsystems.com. (
                       4007121214 ; serial
                       3600       ; refresh (1 hour)
                       900        ; retry (15 minutes)
                       2592000    ; expire (4 weeks 2 days)
                       300        ; minimum (5 minutes)
                       )
````
