# Command `date` - Notes

## Date examples

```bash
    $ date
    Sun Apr 14 15:19:28 EDT 2019

    $ date --date="10 years ago"
    Tue Apr 14 15:19:56 EDT 2009
    
    $ date --date="10 years ago" +%s
    1239736811
    
    $ date --date="yesterday"
    Sat Apr 13 15:20:24 EDT 2019
    
    $ date --date="tomorrow"
    Mon Apr 15 15:20:31 EDT 2019
    
    $ date --date="last tuesday"
    Tue Apr  9 00:00:00 EDT 2019
    
    $ date --date="+3 weeks"
    Sun May  5 15:21:06 EDT 2019
    
    $ date --date="-3 weeks"
    Sun Mar 24 15:21:13 EDT 2019
    
    $ date --date="-3 days"
    Thu Apr 11 15:21:26 EDT 2019
    
    $ date --date="+2 hours"
    Sun Apr 14 17:21:37 EDT 2019
    
    $ date --date="+2 hours +5 years +1 month"
    Tue May 14 17:21:53 EDT 2024
    
    $ date --date='2 days ago' +%Y.%m.%d
    2019.04.12

    $ date -d'next monday' +%Y.%m.%d
    2019.04.15
    
    $ date --date='next monday' +%Y.%m.%d
    2019.04.15

    $ date -d'next monday+14 days' +%Y.%m.%d
    2019.04.29

```

## Print date in epoch time

```bash
$ date +%s
1555264145

 $ date -d'next monday+14 days' +%s
1556510400
 $
```

## Printout date in format suitable for affixing to file names

```bash
date +"%Y%m%d_%H%M%S"
20160615_201606
```

## Epoch Time Conversion function

```bash
#!/bin/bash

date2stamp () {
    date --utc --date "$1" +%s
}

stamp2date (){
    date --utc --date "1970-01-01 $1 sec" "+%Y-%m-%d %T"
}

# convert a date into a UNIX timestamp
    stamp=$(date2stamp "2013-02-01 00:00")
    echo $stamp

# from timestamp to date
    stamp2date $stamp
```

## Get the date for the last Saturday of a given month

If your locale has Monday as the first day of the week, like mine in the UK, change the two $7
into $6

```bash
$ cal 04 2019
     April 2019
Su Mo Tu We Th Fr Sa
    1  2  3  4  5  6
 7  8  9 10 11 12 13
14 15 16 17 18 19 20
21 22 23 24 25 26 27
28 29 30

$ cal 04 2019 | awk '{ $7 && X=$7 } END { print X }'
27
$
```

## Day of the week

The %u format specifier outputs a number from 1 to 7 which corresponds to Monday thru
1/7Sunday. The %w format specifier outputs a number from 0 to 6 which corresponds to Sunday
thru Saturday. The above date specifications may look a bit strange since there is no actual
date, just the day of the week, in this case the date command just assumes you mean the
closest date that falls on the day of the week that you specify

```bash
$ date --date 'Sunday' '+%u'
7 $
date --date 'Sunday' '+%w'
0
```

## Getting Date In the Future
To get tomorrow and day after tomorrow (tomorrow+N) use day word to get date in the future as follows:

```bash
date --date='tomorrow'
date --date='1 day'
date --date='10 day'
date --date='10 week'
date --date='10 month'
date --date='10 year'
```
The date string 'tomorrow' is worth one day in the future which is equivalent to 'day' string i.e. first two commands are same.

## Getting Date In the Past
To get yesterday and earlier day in the past use string day ago:
```bash
date --date='yesterday'
date --date='1 day ago'
date --date='10 day ago'
date --date='10 week ago'
date --date='10 month ago'
date --date='10 year ago'
```
