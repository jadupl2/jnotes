# dpkg Notes

| Command               | Description                        |
|:--------------------- |:-----------------------------------|
dpkg -i vnstat.deb      | Upgrade package if it is installed else install a fresh copy of package
dpkg -R /tmp/downloads  | Install all packages recursively from directory
dpkg -r vnstat          | Remove/Delete an installed package except configuration files
dpkg -P vnstat          | Remove/Delete everything including configuration files
dpkg -l                 | List all installed packages, along with package version & short description
dpkg -l vnstat          | List individual installed packages, along with package version & short description
dpkg -L vnstat          | Find out files are provided by the installed package i.e. list where files were installed
dpkg -c vnstat.deb      | List files provided by the package i.e. List all files inside debian .deb package file
dpkg -S /bin/netstat    | Find what package owns the file i.e. find out what package does file belong
dpkg -p vnstat          | Display details about package, group, version, maintainer, Arch., depends packages, description ...
dpkg -s vnstat \| grep Status | Find out if Debian package is installed or not (status)
