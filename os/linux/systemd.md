# Systemd 

- [Systemd](#systemd)
  - [systemctl command](#systemctl-command)
  - [Install units](#install-units)
  - [Unit types](#unit-types)
  - [hostnamectl command](#hostnamectl-command)
  - [journalctl command](#journalctl-command)
  - [localectl command](#localectl-command)
  - [loginctl command](#loginctl-command)


## systemctl command

|Command                                     | Description   |       |
| :---                                       | :---          | :---: |
| systemctl isolate graphical.target         | Change to runLevel 5 for a graphical interface | 
| systemctl isolate multi-user.target        | Change to runLevel 3 for a command line interface | 
| systemctl                                  | halt	Halt system  | 
| systemctl reboot                           | Reboot system | 
| systemctl poweroff                         | Shutdown and power off system | 
| systemctl suspend	                         | Suspend system | 
| systemctl hibernate                        | Hibernate system | 
| systemctl --type=UNITS or --state=STATE    | List all units on system,separated by comma or type of units.  | 
| systemctl start UNITS                      | Start the designated unit| |
| systemctl stop UNITS                       | Stop the designated unit| |
| systemctl restart UNITS                    | Restart the designated unit| | 
| systemctl daemon-reload UNITS              | Reload the designated unit configuration| | 
| systemctl enable UNITS                     | Enable the designated unit| |
| systemctl disable UNITS                    | Disable the designated unit| |
| systemctl is-enable UNITS                  | Check whether service is configured to start or not| |
| systemctl list-units-files --type=service  | Lists service and runlevels each is configured on or off (ls /etc/systemd/system/*.wants)| |
| systemd-analyze                            | Show bootup time in kernel and userspace ||
| systemd-analyze blame                      | Show how long each service took to start at the last boot ||
| systemd-analyze critical-chain             | Show how each units depend on each other during bootup. ||


## Install units 
```bash
# vi edit <unit file>
   [Unit]
   After=syslog.target
   [Install]
   WantedBy=multi-user.target
# cp <unit> /lib/systemd/system
# systemctl enable <unit>
```



## Unit types
- service
- automount
- timer
- socket
- swap
- snapshot
- device
- target
- slice
- mount
- path
- scope



## hostnamectl command

|Command                                     | Description   |       |
| :---                                       | :---          | :---: |
| hostnamectl                                | Queries and changes the system hostname. | |

## journalctl command

|Command                                     | Description   |       |
| :---                                       | :---          | :---: |
| journalctl -o                              | Show full text of each entry (default is a shortened version) | |
| journalctl -p PRIORITY                     | Mess with given priority (emerg(0),alert(1),crit(2),err(3),warnig(4),notice(5),info(6),debug(7))| |
| journalctl -b                              | Log since most recent boot | |
| journalctl --since yesterday               | Log since yesterday | |
| journalctl --reverse                       | Entries in reverse order, latest entry first. | |
| journalctl -u cron.service --since="YYYY-MM-DD HH:MM" --until="YYYY-MM-DD HH:MM" | Log defined for a defined period. | |
| journalctl -f                              | Live depiction of journal, similar to tail command | |
| journalctl --follow                        | Only the last 10 most recent entries | |
| journalctl -f                              | Live depiction of journal, similar to tail command | |
| journalctl -f                              | Live depiction of journal, similar to tail command | |

## localectl command

|Command                                     | Description   |       |
| :---                                       | :---          | :---: |
| localectl                                  | Sets system locale and keyboard| |

## loginctl command

|Command                                     | Description   |       |
| :---                                       | :---          | :---: |
| loginctl                                   | Sets login processes| |

