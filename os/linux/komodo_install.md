## Installing Komodo on Linux

http://docs.activestate.com/komodo/11/get/linux/

This version of Komodo allows non-root installation on Linux. Note, however, that the user who
executes the license file will be the user who is licensed to use the software.
To install Komodo on Linux:
1. Download the Komodo installer (.tar.gz file) into a convenient directory.
2. Unpack the tarball:
3. Change to the new directory:
    `cd Komodo-<version>-<platform>`
4. Run the install script ( install.sh ):
    `./install.sh`
5. Answer the installer prompts:
6. Specify where you want Komodo installed, or press ‘Enter’ to accept the default location

( /home/<username>/Komodo-<IDE|Edit>-x.y ).
The -I option can be used to specify the install directory. For example:
    `./install.sh -I ~/opt/Komodo-IDE-11`

If multiple users are sharing the system and will be using the same installation, install Komodo in
a location every user can access (e.g. /opt/Komodo-x.x/ or /usr/local/Komodo-x.x/ ).

#### Note:
! Each Komodo user requires their own license key.
! Do not install Komodo in a path that contains spaces or non-alphanumeric characters.
! Be sure to install Komodo into its own directory (i.e. not directly in an existing directory
containing shared files and directories such as /usr/local).

Once the installer has finished, add Komodo to your PATH with one of the following:
! Add Komodo/bin to your PATH directly:
    `export PATH=<installdir>/bin:$PATH`

! Add a symlink to Komodo/bin/komodo from another directory in your PATH:
    `ln -s <installdir>/bin/komodo /usr/local/bin/komodo`

#### Note: Creating symlinks in system directories such as /usr/bin requires root access.
After completing the installation, you can delete the temporary directory where the Komodo
tarball was unpacked.

### Starting Komodo on Linux
To start Komodo on Linux enter komodo at the command line or create a shortcut on your
desktop or in your toolbar using the full path to the komodo executable.

### Uninstalling Komodo on Linux

To uninstall Komodo on Linux:
1. Delete the directory that Komodo created during installation.
2. If you wish to delete your Komodo preferences, delete the ~/.komodo directory. If you do not
delete this directory, subsequent installations of Komodo will use the same preferences.

####Note: You cannot relocate an existing Komodo installation to a new directory by simply moving it.
You must uninstall Komodo from the existing location and reinstall it in the new location.