# Broadcom Corporation NetXtreme BCM5702X Gigabit Ethernet
# --------------------------------------------------------

# Interface name
DEVICE=eth0

# Boot Protocol none,bootp,dhcp
BOOTPROTO=none

# Hardware Mac Address
HWADDR=00:0D:56:77:C0:F4

# Activate at boot time
ONBOOT=yes

# Type ethernet, tokenring
TYPE=Ethernet

# yes = Non-root users allowed to control this device.
# no  = Non-root users not allowed to control this device.
USERCTL=no

# IPv6 Actvated
IPV6INIT=no

# yes = Modify /etc/resolv.conf if DNS directive is set. 
#       If using DCHP, then yes is the default.
# no  = Do not modify /etc/resolv.conf
PEERDNS=no

# The subnet mask of the network
NETMASK=255.255.255.0

# Interface's IP address
IPADDR=192.168.1.135

# Network Gateway
GATEWAY=192.168.1.1

# Set Netword Card characteristics 
ETHTOOL_OPTS="autoneg off speed 100 duplex full"


[write font:AvenirNext-Regular size:16.00 bg:{1.0000, 1.0000} end]