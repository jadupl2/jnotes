# MyNotes - vim editor

## List Vim Color Schemes

- Before selecting a color scheme we need to list vim provided color schemes.  
- Vim color schemes are stored in vim directory named /usr/share/vim/vim80/colors/ but vim80 can be different according to vim version.  
- Color schemes have .vim extension.  
- We can use ls -l command in order to print available color schemes.  

```
jacques@holmes:~$ ls -l /usr/share/vim/vim*/colors/
/usr/share/vim/vim74/colors/:
total 72
-rw-r--r-- 1 root root 2476 Oct 30 15:57 blue.vim
-rw-r--r-- 1 root root 2990 Oct 30 15:57 darkblue.vim
-rw-r--r-- 1 root root  548 Oct 30 15:57 default.vim
-rw-r--r-- 1 root root 2399 Oct 30 15:57 delek.vim
-rw-r--r-- 1 root root 2812 Oct 30 15:57 desert.vim
-rw-r--r-- 1 root root 1666 Oct 30 15:57 elflord.vim
-rw-r--r-- 1 root root 2476 Oct 30 15:57 evening.vim
-rw-r--r-- 1 root root 3476 Oct 30 15:57 koehler.vim
-rw-r--r-- 1 root root 2460 Oct 30 15:57 morning.vim
-rw-r--r-- 1 root root 2006 Oct 30 15:57 murphy.vim
-rw-r--r-- 1 root root 1037 Oct 30 15:57 pablo.vim
-rw-r--r-- 1 root root 2673 Oct 30 15:57 peachpuff.vim
-rw-r--r-- 1 root root 2311 Oct 30 15:57 README.txt
-rw-r--r-- 1 root root 1393 Oct 30 15:57 ron.vim
-rw-r--r-- 1 root root 2720 Oct 30 15:57 shine.vim
-rw-r--r-- 1 root root 2445 Oct 30 15:57 slate.vim
-rw-r--r-- 1 root root 1629 Oct 30 15:57 torte.vim
-rw-r--r-- 1 root root 1840 Oct 30 15:57 zellner.vim

/usr/share/vim/vimfiles/colors/:
total 0
jacques@holmes:~$
```

## Ccycle through the theme. 

```
:colorscheme
```
then SPACE and TAB


## Make Color scheme Persistent Specifying  vimrc

- Changes are not saved between vim sessions. 
- We can save this configuration to the  vimrc configuration file and made it persistent. 
- vimrc is located at /etc or users home directory.  
```
echo ":colorscheme delek" >> ~/.vimrc
```
