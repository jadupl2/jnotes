# QLOGIC HBA FIBRE CARD - My Notes

## Get number of card and ports (2 dual ports hba)

```bash
root:~# lspci | grep -i fibre
07:00.0 Fibre Channel: QLogic Corp. ISP2532-based 8Gb Fibre Channel to PCI Express HBA (rev 02)
07:00.1 Fibre Channel: QLogic Corp. ISP2532-based 8Gb Fibre Channel to PCI Express HBA (rev 02)
24:00.0 Fibre Channel: QLogic Corp. ISP2532-based 8Gb Fibre Channel to PCI Express HBA (rev 02)
24:00.1 Fibre Channel: QLogic Corp. ISP2532-based 8Gb Fibre Channel to PCI Express HBA (rev 02)
```

## List world wide name of hba (rhel 5 and 6) 

Here we see that the first and third wwn are online (connected to brocade)

```bash
root:# nl /sys/class/fc_host/host[0-9]/port_name
     1  0x21000024ff39fa8e
     2  0x21000024ff39fa8f
     3  0x500143802422a904
     4  0x500143802422a906
root:# nl /sys/class/fc_host/host[0-9]/port_state
     1  Online
     2  Linkdown
     3  Online
     4  Linkdown

root:/sys/class/fc_host/host0 # cat symbolic_name 
QLE2560 FW:v5.03.16 DVR:v8.03.07.03.05.07-k

root:/sys/class/fc_host/host0 # cat supported_speeds 
1 Gbit, 2 Gbit, 4 Gbit, 8 Gbit

root:/sys/class/fc_host/host0 # cat speed 
4 Gbit

root:/sys/class/fc_host/host0 # cat node_name 
0x2000001b328f31a6

root:/sys/class/fc_host/host0 # cat port_name 
0x2100001b328f31a6

root:/sys/class/fc_host/host0 # cat port_state
Online

root:/sys/class/fc_host/host0 # cat port_type
NPort (fabric via point-to-point)
```

## link with san box

```bash
root:/sys/class/fc_transport/target1:0:0 # cat port_name 
0x50060e80132dab00

root:/sys/class/fc_transport # ls -l
total 0
drwxr-xr-x 2 root root 0 Jul 14 14:41 target1:0:0
drwxr-xr-x 2 root root 0 Jul 14 13:27 target2:0:0

root:/sys/class/fc_transport # cd target2\:0\:0/

root:/sys/class/fc_transport/target2:0:0 # cat port_name
0x50060e80132dab10

root:/sys/class/scsi_host/host0 # cat fw_version 
5.03.16 (95)

root:/sys/class/scsi_host/host0 # cat driver_version 
8.03.07.03.05.07-k

root:/sys/class/scsi_host/host0 # cat model_desc 
PCI-Express Single Channel 8Gb Fibre Channel HBA

root:/sys/class/scsi_host/host0 # cat model_name 
QLE2560
```

## Make sure the qlogic configuration file exist and contains the line below

```
root# cat /etc/modprobe.d/qlogic.conf 
options qla2xxx  ql2xmaxlun=255
```
