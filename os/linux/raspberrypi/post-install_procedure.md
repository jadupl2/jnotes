# Raspberry Pi - Post Installation Procedure #

### Log to the newly installed Raspberry Pi 
We previously installed Raspbian on that Raspberry Pi, so there is the old SSH key in the know_hosts file.

```bash
jacques@iMac:~ $ ssh -p22 pi@raspi3
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:aSuSjMnncf2Imzc7YNVrgkjvnyT+IbEzll9P09l9dgE.
Please contact your system administrator.
Add correct host key in /Users/jacques/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /Users/jacques/.ssh/known_hosts:15
ECDSA host key for raspi3 has changed and you have requested strict checking.
Host key verification failed.
jacques@iMac:~ $
````

## Remove the old ssh key on our server
(Delete line 15)
```bash
$ sudo vi +15 /root/.ssh/known_hosts  
````

## Let's ssh to our newly installed Raspberry Pi
```bash
root@holmes:/install/iso# ssh -p 22 pi@raspi3
pi@raspi3's password:
The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the individual files in /usr/share/doc/*/copyright. Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law.
Last login: Sat May  6 09:17:05 2017

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Fri Oct 19 07:32:16 2018
pi@raspi3:~ $
`````

## Changing the 'root' user password
````bash
pi@raspi3:~ $ sudo su -
root@raspi3:~# passwd
Enter new UNIX password: XXXXXXXX
Retype new UNIX password: XXXXXXXX
passwd: password updated successfully
root@raspi3:~#
`````

## Start Post-Installation
````bash
root@raspi3:~# mkdir /mnt/nfs
root@raspi3:~# chmod 755 /mnt/nfs
root@raspi3:~# mount holmes:/install /mnt/nfs
root@raspi3:~# /mnt/nfs/sadm_postinstall.sh
````

## End of Post-Installation
At the end of the post-installation script the system will reboot.
````bash
2018-10-19 16:12 -
2018-10-19 16:12 - End of SADM_Postscript file
2018-10-19 16:12 -
2018-10-19 16:12 -
2018-10-19 16:12 - Rebooting ...
2018-10-19 16:12 -
2018-10-19 16:12 -
Connection to raspi3 closed by remote host.
Connection to raspi3 closed.
`````

### Looking at the log 
<br>You can take a look at the install log and check if everytginh went ok.
<br>The installation is located at "/root/postinstall.log".
````bash
root@raspi3~# less postinstall.log
Checking if lsb_release is available
The command lsb_release is used to determine the distribution name and version
Installation of lsb_release succeeded - Continue the post-installation

2018-10-19 16:00 -
2018-10-19 16:00 - ========== Start of postinstall script
2018-10-19 16:00 - SCRIPT VERSION  = 3.8
2018-10-19 16:00 - OS_VERSION      = 9
2018-10-19 16:00 - OS_BITS         = 32
2018-10-19 16:00 - SRC_DIR         = /mnt/nfs
2018-10-19 16:00 - STD_FILES       = /mnt/nfs/files
2018-10-19 16:00 - SOFTWARE        = /mnt/nfs/software
2018-10-19 16:00 - LOG_DIR         = /root
2018-10-19 16:00 - LOG_FILE        = /root/postinstall.log
2018-10-19 16:00 - STORIX_SERVER   = storix.maison.ca
2018-10-19 16:00 - DOMAIN          = maison.ca
2018-10-19 16:00 - HOSTNAME        = raspi3
2018-10-19 16:00 - /bin/hostname   = raspi3.maison.ca
2018-10-19 16:00 - IPADDR          = 192.168.1.17
2018-10-19 16:00 - NETMASK         = 255.255.255.0
2018-10-19 16:00 - GATEWAY         = 192.168.1.1
2018-10-19 16:00 - ETHDEV          = eth0
2018-10-19 16:00 - BROADCAST       = 192.168.1.255
2018-10-19 16:00 - NETWORK         = 192.168.1.0
2018-10-19 16:00 - NAME_SERVERS    = 192.168.1.126 192.168.1.127 8.8.8.8
2018-10-19 16:00 - VMWARE ?        = N
2018-10-19 16:00 - OS_NAME         = RASPBIAN
2018-10-19 16:00 - MAIL_RELAYHOST  = relais.videotron.ca
2018-10-19 16:00 - SYSTEMD ENABLE  = 1 (0=Sysinit 1=Systemd)
2018-10-19 16:00 -
2018-10-19 16:00 -
`````
