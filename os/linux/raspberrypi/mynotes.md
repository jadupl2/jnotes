# Raspberry Pi Notes

### fsck at every boot

My Raspberry Pi's have a cronjob which reboots them once every seven days. This to apply
kernel updates and just a general good procedure to see if all still works after a reboot. By
default, fsck checks a filesystem every 30 boots (counted individually for each partition). I
decided to change this to every boot, so problems will be found and possibly fixed earlier.
To set up an fsck at every boot, execute the following command:

````
tune2fs -c 1 /dev/sda1
````

Where /dev/sda1 is the device or partition.
Even Better 

````
root@raspi3~# vi /lib/init/vars.sh
````

Change FSCKFIX=no to FSCKFIX=yes