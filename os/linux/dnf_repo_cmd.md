## dnf notes

### Show system repositories list
```bash
[root@rhel8beta yum.repos.d]# dnf repolist
Updating Subscription Management repositories.
Updating Subscription Management repositories.
Last metadata expiration check: 3:04:38 ago on Sat 02 Mar 2019 07:27:41 AM EST.
repo id                               repo name                                                    status
code                                  Visual Studio Code                                              54
rhel-8-for-x86_64-appstream-beta-rpms Red Hat Enterprise Linux 8 for x86_64 - AppStream Beta (RPMs 4,594
rhel-8-for-x86_64-baseos-beta-rpms    Red Hat Enterprise Linux 8 for x86_64 - BaseOS Beta (RPMs)   1,686
[root@rhel8beta yum.repos.d]# 
```

[root@rhel8beta yum.repos.d]# cat vscode.repo 
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
[root@rhel8beta yum.repos.d]# 
[root@rhel8beta yum.repos.d]# 
[root@rhel8beta yum.repos.d]# dnf config-manager --set-disabled code
Updating Subscription Management repositories.
Updating Subscription Management repositories.
[root@rhel8beta yum.repos.d]# 
[root@rhel8beta yum.repos.d]# 
[root@rhel8beta yum.repos.d]# cat vscode.repo 
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=0
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
[root@rhel8beta yum.repos.d]# 
[root@rhel8beta yum.repos.d]# dnf config-manager --set-enabled code
Updating Subscription Management repositories.
Updating Subscription Management repositories.
[root@rhel8beta yum.repos.d]# 
[root@rhel8beta yum.repos.d]# 
[root@rhel8beta yum.repos.d]# cat vscode.repo 
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
[root@rhel8beta yum.repos.d]# 









6.5. Adding, Enabling, and Disabling a DNF Repository
Section 6.3.2, “Setting [repository] Options” describes various options you can use to define a DNF repository. This section explains how to add, enable, and disable a repository by using the dnf config-manager command.
Adding a DNF Repository
To define a new repository, you can either add a [repository] section to the /etc/dnf/dnf.conf file, or to a .repo file in the /etc/yum.repos.d/ directory. All files with the .repo file extension in this directory are read by DNF, and it is recommended to define your repositories here instead of in /etc/dnf/dnf.conf.
DNF repositories commonly provide their own .repo file. To add such a repository to your system and enable it, run the following command as root:

dnf config-manager --add-repo repository_url

…where repository_url is a link to the .repo file.
Example 6.8. Adding example.repo
To add a repository located at http://www.example.com/example.repo, type the following at a shell prompt:

~]# dnf config-manager --add-repo http://www.example.com/example.repo
adding repo from: http://www.example.com/example.repo


Enabling a DNF Repository
To enable a particular repository or repositories, type the following at a shell prompt as root:

dnf config-manager --set-enabled repository…

…where repository is the unique repository ID. To display the current configuration, add the --dump option.
Disabling a DNF Repository
To disable a DNF repository, run the following command as root:

dnf config-manager --set-disabled repository…

…where repository is the unique repository ID. To display the current configuration, add the --dump option. 




6.3.3. Using DNF Variables
Variables can be used only in the appropriate sections of the DNF configuration files, namely the /etc/dnf/dnf.conf file and all .repo files in the /etc/yum.repos.d/ directory. Repository variables include:

$releasever
    Refers to the release version of operating system which DNF derives from information available in RPMDB. 
$arch
    Refers to the system’s CPU architecture. Valid values for $arch include: i586, i686 and x86_64. 
$basearch
    Refers to the base architecture of the system. For example, i686 and i586 machines both have a base architecture of i386, and AMD64 and Intel64 machines have a base architecture of x86_64. 
    
    
    

~]$ dnf config-manager --dump
=============================== main ======================================
[main]
alwaysprompt = True
assumeno = False
assumeyes = False
bandwidth = 0
best = False
bugtracker_url = https://bugzilla.redhat.com/enter_bug.cgi?product=Fedora&component=dnf
cachedir = /var/cache/dnf/x86_64/22
[output truncated]





6.3.1. Setting [main] Options
The /etc/dnf/dnf.conf configuration file contains exactly one [main] section, and while some of the key-value pairs in this section affect how dnf operates, others affect how DNF treats repositories. You can add many additional options under the [main] section heading in /etc/dnf/dnf.conf.
A sample /etc/dnf/dnf.conf configuration file can look like this:

[main]
gpgcheck=1
installonly_limit=3
clean_requirements_on_remove=true

The following are the most commonly-used options in the [main] section:

debuglevel=value
    …where value is an integer between 0 and 10. Setting a higher debuglevel value causes dnf to display more detailed debugging output. debuglevel=0 disables debugging output, and debuglevel=2 is the default. 
exclude=package_name [more_package_names]
    This option allows you to exclude packages by keyword during installation and updates. Listing multiple packages for exclusion can be accomplished by quoting a space-delimited list of packages. Shell globs using wildcards (for example, * and ?) are allowed. 
gpgcheck=value
    …where value is one of:
    0 — Disable GPG signature-checking on packages in all repositories, including local package installation.
    1 — Enable GPG signature-checking on all packages in all repositories, including local package installation. gpgcheck=1 is the default, and thus all packages' signatures are checked.
    If this option is set in the [main] section of the /etc/dnf/dnf.conf file, it sets the GPG-checking rule for all repositories. However, you can also set gpgcheck=value for individual repositories instead; you can enable GPG-checking on one repository while disabling it on another. Setting gpgcheck=value for an individual repository in its corresponding .repo file overrides the default if it is present in /etc/dnf/dnf.conf.
    For more information on GPG signature-checking, refer to Section A.3.2, “Checking Package Signatures”. 
installonlypkgs=space separated list of packages
    Here you can provide a space-separated list of packages which dnf can install, but will never update. See the dnf.conf(5) manual page for the list of packages which are install-only by default.
    If you add the installonlypkgs directive to /etc/dnf/dnf.conf, you should ensure that you list all of the packages that should be install-only, including any of those listed under the installonlypkgs section of dnf.conf(5). In particular, kernel packages should always be listed in installonlypkgs (as they are by default), and installonly_limit should always be set to a value greater than 2 so that a backup kernel is always available in case the default one fails to boot. 
installonly_limit=value
    …where value is an integer representing the maximum number of versions that can be installed simultaneously for any single package listed in the installonlypkgs directive.
    The defaults for the installonlypkgs directive include several different kernel packages, so be aware that changing the value of installonly_limit will also affect the maximum number of installed versions of any single kernel package. The default value listed in /etc/dnf/dnf.conf is installonly_limit=3, and it is not recommended to decrease this value, particularly below 2. 
keepcache=value
    …where value is one of:
    0 — Do not retain the cache of headers and packages after a successful installation. This is the default.
    1 — Retain the cache after a successful installation. 

For a complete list of available [main] options, refer to the [MAIN] OPTIONS section of the dnf.conf(5) manual page. 



 dnf list glob_expression…
    Lists information on installed and available packages matching all glob expressions.
    Example 6.1. Listing all ABRT addons and plug-ins using glob expressions
    Packages with various ABRT addons and plug-ins either begin with “abrt-addon-”, or “abrt-plugin-”. To list these packages, type the following at a shell prompt:

    ~]# dnf list abrt-addon\* abrt-plugin\*
    Last metadata expiration check performed 0:14:36 ago on Mon May 25 23:38:13 2015.
    Installed Packages
    abrt-addon-ccpp.x86_64                  2.5.1-2.fc22               @System
    abrt-addon-coredump-helper.x86_64       2.5.1-2.fc22               @System
    abrt-addon-kerneloops.x86_64            2.5.1-2.fc22               @System
    abrt-addon-pstoreoops.x86_64            2.5.1-2.fc22               @System
    abrt-addon-python.x86_64                2.5.1-2.fc22               @System
    abrt-addon-python3.x86_64               2.5.1-2.fc22               @System
    abrt-addon-vmcore.x86_64                2.5.1-2.fc22               @System
    abrt-addon-xorg.x86_64                  2.5.1-2.fc22               @System
    abrt-plugin-bodhi.x86_64                2.5.1-2.fc22               @System
    Available Packages
    abrt-addon-upload-watch.x86_64          2.5.1-2.fc22               fedora


dnf list all
    Lists all installed and available packages.
    Example 6.2. Listing all installed and available packages

    ~]# dnf list all
    Last metadata expiration check performed 0:21:11 ago on Mon May 25 23:38:13 2015.
    Installed Packages
    NetworkManager.x86_64                   1:1.0.2-1.fc22             @System
    NetworkManager-libnm.x86_64             1:1.0.2-1.fc22             @System
    PackageKit.x86_64                       1.0.6-4.fc22               @System
    PackageKit-glib.x86_64                  1.0.6-4.fc22               @System
    aajohan-comfortaa-fonts.noarch          2.004-4.fc22               @System
    abrt.x86_64                             2.5.1-2.fc22               @System
    [output truncated]


dnf list installed
    Lists all packages installed on your system. The rightmost column in the output lists the repository from which the package was retrieved.
    Example 6.3. Listing installed packages using a double-quoted glob expression
    To list all installed packages that begin with “krb” followed by exactly one character and a hyphen, type:

    ~]# dnf list installed "krb?-*"
    Last metadata expiration check performed 0:34:45 ago on Mon May 25 23:38:13 2015.
    Installed Packages
    krb5-libs.x86_64                        1.13.1-3.fc22              @System
    krb5-workstation.x86_64                 1.13.1-3.fc22              @System


dnf list available
    Lists all available packages in all enabled repositories.
    Example 6.4. Listing available packages using a single glob expression with escaped wildcard characters
    To list all available packages with names that contain “gstreamer” and then “plugin”, run the following command:

    ~]# dnf list available gstreamer\*plugin\*
    Last metadata expiration check performed 0:42:15 ago on Mon May 25 23:38:13 2015.
    Available Packages
    gstreamer-plugin-crystalhd.i686              3.10.0-8.fc22          fedora
    gstreamer-plugin-crystalhd.x86_64            3.10.0-8.fc22          fedora
    gstreamer-plugins-bad-free.i686              0.10.23-24.fc22        fedora
    gstreamer-plugins-bad-free.x86_64            0.10.23-24.fc22        fedora
    gstreamer-plugins-bad-free-devel.i686        0.10.23-24.fc22        fedora
    gstreamer-plugins-bad-free-devel.x86_64      0.10.23-24.fc22        fedora
     [output truncated]


dnf group list
    Lists all package groups.
    Example 6.5. Listing all package groups

    ~]# dnf group list
    Loaded plugins: langpacks, presto, refresh-packagekit
    Setting up Group Process
    Installed Groups:
       Administration Tools
       Design Suite
       Dial-up Networking Support
       Fonts
       GNOME Desktop Environment
    [output truncated]


dnf repolist
    Lists the repository ID, name, and number of packages it provides for each enabled repository.
    Example 6.6. Listing enabled repositories

    ~]# dnf repolist
    Last metadata expiration check performed 0:48:29 ago on Mon May 25 23:38:13 2015.
    repo id                             repo name                           status
    *fedora                             Fedora 22 - x86_64                  44,762
    *updates                            Fedora 22 - x86_64 - Updates             0


dnf repository-packages repo_id list
    Lists the packages from the specified repository.
    Example 6.7. Listing packages from a single repository

    ~]# dnf repository-packages fedora list [option]
    Last metadata expiration check performed 1:38:25 ago on Wed May 20 22:16:16 2015.
    Installed Packages
    PackageKit.x86_64                        1.0.6-3.fc22                    @System
    PackageKit-glib.x86_64                   1.0.6-3.fc22                    @System
    aajohan-comfortaa-fonts.noarch           2.004-4.fc22                    @System
    [output truncated]

    The default action is to list all packages available and installed from the repository specified. Add the available or installed option to list only those packages available or installed from the specified repository.

    Prev
    6.2. Packages and Package Groups
    UpHome
    
    
    


6.3.2. Setting [repository] Options
The [repository] sections, where repository is a unique repository ID such as my_personal_repo (spaces are not permitted), allow you to define individual DNF repositories.
The following is a bare-minimum example of the form a [repository] section takes:

[repository]
name=repository_name
baseurl=repository_url

Every [repository] section must contain the following directives:

name=repository_name
    …where repository_name is a human-readable string describing the repository. 
parameter=repository_url
    …where parameter is one of the following: baseurl, metalink, or mirrorlist;
    …where repository_url is a URL to a directory containing a repodata directory of a repository, a metalink file, or a mirror list file.

        If the repository is available over HTTP, use: http://path/to/repo
        If the repository is available over FTP, use: ftp://path/to/repo
        If the repository is local to the machine, use: file:///path/to/local/repo
        If a specific online repository requires basic HTTP authentication, you can specify your user name and password by prepending it to the URL as username:password@link. For example, if a repository on http://www.example.com/repo/ requires a username of “user” and a password of “password”, then the baseurl link could be specified as http://user:password@www.example.com/repo/. 

    Usually this URL is an HTTP link, such as:

    baseurl=http://path/to/repo/releases/$releasever/server/$basearch/os/

    Note that DNF always expands the $releasever, $arch, and $basearch variables in URLs. For more information about DNF variables, refer to Section 6.3.3, “Using DNF Variables”. 

To configure the default set of repositories, use the enabled option as follows:

enabled=value
    …where value is one of:
    0 — Do not include this repository as a package source when performing updates and installs.
    1 — Include this repository as a package source.
    Turning repositories on and off can also be performed by passing either the --set-enabled repo_name or --set-disabled repo_name option to the dnf command, or through the Add/Remove Software window of the PackageKit utility. 

Many more [repository] options exist. For a complete list, refer to the [repository] OPTIONS section of the dnf.conf(5) manual page. 



