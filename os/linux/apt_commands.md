# dpkg/apt/apt-get Notes  <!-- omit in toc -->


- [List of all available packages](#list-of-all-available-packages)
- [List all packages installed](#list-all-packages-installed)
- [Remove a Package](#remove-a-package)
- [Reinstall a package](#reinstall-a-package)
- [Correcting Hash Sum Mismatch](#correcting-hash-sum-mismatch)
- [Avoiding the conffile prompt when installing or updating a .deb](#avoiding-the-conffile-prompt-when-installing-or-updating-a-deb)
- [What is the package name that provide the command](#what-is-the-package-name-that-provide-the-command)
  - [If the command is installed](#if-the-command-is-installed)
  - [If the command is not installed](#if-the-command-is-not-installed)
  - [Using 'apt-file search' to find the package name of the command](#using-apt-file-search-to-find-the-package-name-of-the-command)
- [apt-get clean](#apt-get-clean)
- [apt-get autoclean](#apt-get-autoclean)
- [apt-key list](#apt-key-list)



## List of all available packages

```bash
# apt-get update
# apt-cache dump | grep -oP 'Package: \K.*' | grep -i "^keep"
keepalived
keepalived:i386
keepass2
keepass2-doc
keepassx
keepassx:i386
keepnote
```

## List all packages installed

```bash
# dpkg --list | grep cfg2html
  cfg2html-linux    2.54-1  all   Collects Linux system configuration into a HTML and text file.
```


## Remove a Package

```bash
# apt-get remove cfg2html-linux
Reading package lists... Done
Building dependency tree  	 
Reading state information... Done
The following packages were automatically installed and are no longer required:
  apt-clone archdetect-deb dpkg-repack gir1.2-json-1.0 gir1.2-timezonemap-1.0 gir1.2-xkl-1.0 libdebian-installer4
  libparted-fs-resize0 libtimezonemap-data libtimezonemap1 os-prober python3-icu python3-pam rdate
Use 'apt-get autoremove' to remove them.
The following packages will be REMOVED:
  cfg2html-linux
0 upgraded, 0 newly installed, 1 to remove and 0 not upgraded.
After this operation, 385 kB disk space will be freed.
Do you want to continue? [Y/n] y
(Reading database ... 166947 files and directories currently installed.)
Removing cfg2html-linux (2.54-1) ...
Processing triggers for man-db (2.7.4-1) ...
#
If you even want to remove configuration file
# apt-get --purge remove octave3.2
```


## Reinstall a package

```bash
root@ubuntu1604~# apt-get install --reinstall ubuntu-desktop
Reading package lists... Done
Building dependency tree  	 
Reading state information... Done
The following packages were automatically installed and are no longer required:
  linux-headers-4.4.0-31 linux-headers-4.4.0-31-generic
  linux-image-4.4.0-31-generic linux-image-extra-4.4.0-31-generic
Use 'apt autoremove' to remove them.
0 upgraded, 0 newly installed, 1 reinstalled, 0 to remove and 0 not upgraded.
Need to get 0 B/3,616 B of archives.
After this operation, 0 B of additional disk space will be used.
(Reading database ... 155970 files and directories currently installed.)
Preparing to unpack .../ubuntu-desktop_1.361_amd64.deb ...
Unpacking ubuntu-desktop (1.361) over (1.361) ...
Setting up ubuntu-desktop (1.361) ...
root@ubuntu1604~#
```


## Correcting Hash Sum Mismatch

W: Failed to fetch gzip:/var/lib/apt/lists/partial/us.archive.ubuntu.com_ubuntu_dists_natty_main_source_Sources Hash Sum mismatch,

E: Some index files failed to download.   
They have been ignored, or old ones used instead.  

The easiest way to fix this is:

```bash
# sudo apt-get clean**
# sudo apt-get update**
```

OR remove all the content of /var/lib/apt/lists directory:
```bash
# sudo rm -rf /var/lib/apt/lists/***
# sudo apt-get update**
```


## Avoiding the conffile prompt when installing or updating a .deb

**Avoiding this kind of message during the O/S update**

```bash
Samba server and utilities

A new version (/run/samba/upgrades/smb.conf) of configuration file  
/etc/samba/smb.conf is available, but the version installed currently has been locally modified.  

  1. install the package maintainer's version  
  2. keep the local version currently installed  
  3. show the differences between the versions
  4. show a side-by-side difference between the versions
  5. show a 3-way difference between available versions
  6. do a 3-way merge between available versions (experimental)
  7. start a new shell to examine the situation

What do you want to do about modified configuration file smb.conf?
```

Every time that dpkg must install a new conffile that you have modified (and a removed file is only 
a particular case of a modified file in dpkg's eyes), it will stop the upgrade and wait your answer. 
This can be particularly annoying for major upgrades.  
That's why you can give predefined answers to dpkg with the help of multiple --force-conf* options:

*   **--force-confold**: do not modify the current configuration file, the new version is installed with a .dpkg-dist suffix.  
    With this option alone, even configuration files that you have not modified are left untouched.  
    You need to combine it with --force-confdef to let dpkg overwrite configuration files that you have not modified. 
*   **--force-confnew:** always install the new version of the configuration file, the current  
    version is kept in a file with the .dpkg-old suffix.
*   **--force-confdef**: ask dpkg to decide alone when it can and prompt otherwise.  
    This is the default behavior of dpkg and this option is mainly useful in combination with --force-confold.
*   **--force-confmiss**: ask dpkg to install the configuration file if it's currently missing  
    (for example because you have removed the file by mistake).

If you use Apt, you can pass options to dpkg with a command-line like this:  

```bash
$ apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade  
$ apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade  
```
You can also make those options permanent by creating /etc/apt/apt.conf.d/local:  

```bash
Dpkg::Options {  
   "--force-confdef";   
   "--force-confold";  

Example :  dpkg --install --force-confold /sadmin/pkg/cfg2html/cfg2html.deb
```

## What is the package name that provide the command 


### If the command is installed

```bash
    # dpkg -S which
    debianutils: /usr/share/man/fr/man1/which.1.gz
    pypy-lib: /usr/lib/pypy/lib-python/2.7/whichdb.py
    emacs24-common: /usr/share/emacs/24.4/lisp/progmodes/which-func.elc
    debianutils: /usr/share/man/pl/man1/which.1.gz
    debianutils: /usr/share/man/it/man1/which.1.gz
    debianutils: /usr/share/man/ja/man1/which.1.gz
    libruby2.1:armhf: /usr/lib/ruby/2.1.0/rubygems/commands/which_command.rb
    debianutils: /usr/share/man/de/man1/which.1.gz
    debianutils: /usr/bin/which
    debianutils: /usr/share/man/sl/man1/which.1.gz
    debianutils: /bin/which
    debianutils: /usr/share/man/es/man1/which.1.gz
    debianutils: /usr/share/man/man1/which.1.gz
    root@raspi4:/disk01 #
```

### If the command is not installed

```bash
# type mail
-bash: type: mail: not found  
#  dpkg -S /usr/bin/mail
dpkg-query: no path found matching pattern /usr/bin/mail
#
```

You can go on another system to identify where the command is located

```bash
# type mail
    mail is hashed (/usr/bin/mail)
# ls -l /usr/bin/mail
   lrwxrwxrwx 1 root root 22 Dec 19 08:10 /usr/bin/mail -> /etc/alternatives/mail  
# ls -l /etc/alternatives/mail
lrwxrwxrwx 1 root root 23 Dec 19 08:10 /etc/alternatives/mail -> /usr/bin/mail.mailutils  
# dpkg -S /usr/bin/mail.mailutils
mailutils: /usr/bin/mail.mailutils
```

### Using 'apt-file search' to find the package name of the command

**Searching for the "man" binary**


```bash
root@raspi3w:~# apt-file search man
The program 'apt-file' is currently not installed. You can install it by typing:
apt-get install apt-file

root@raspi3w:~# apt-get install apt-file
Reading package lists... Done
Building dependency tree  	 
Reading state information... Done
Setting up libregexp-assemble-perl (0.35-8) ...
Setting up apt-file (2.5.4ubuntu1) ...
The system-wide cache is empty. You may want to run 'apt-file update'
as root to update the cache. You can also run 'apt-file update' as
normal user to use a cache in the user's home directory.
```

```bash
root@raspi3w:~# apt-file update
Downloading complete file http://ports.ubuntu.com/dists/wily/Contents-armhf.gz
  % Total	% Received % Xferd  Average Speed   Time	Time 	Time  Current
                             	Dload  Upload   Total   Spent	Left  Speed
100 31.0M  100 31.0M	0 	0   442k  	0  0:01:11  0:01:11 --:--:--  901k
Downloading complete file http://ports.ubuntu.com/ubuntu-ports/dists/wily-security/Contents-armhf.gz
  % Total	% Received % Xferd  Average Speed   Time	Time 	Time  Current
                             	Dload  Upload   Total   Spent	Left  Speed
100 2269k  100 2269k	0 	0   334k  	0  0:00:06  0:00:06 --:--:--  417k
Ignoring source without Contents File:
  http://ppa.launchpad.net/flexiondotorg/minecraft/ubuntu/dists/wily/Contents-armhf.gz
  http://ppa.launchpad.net/ubuntu-pi-flavour-makers/ppa/ubuntu/dists/wily/Contents-armhf.gz
```

```bash
root@ubuntu1604/etc# apt-file search man | grep 'bin/man$'
man-db: /usr/bin/man
root@ubuntu1604/etc#

root@ubuntu1604/etc# apt-get install man-db
Reading package lists... Done
Building dependency tree  	 
The following additional packages will be installed:
  bsdmainutils groff-base libpipeline1
The following NEW packages will be installed:
  bsdmainutils groff-base libpipeline1 man-db
0 upgraded, 4 newly installed, 0 to remove and 80 not upgraded.
Need to get 2,204 kB of archives.
After this operation, 6,094 kB of additional disk space will be used.
Do you want to continue? [Y/n]
Setting up libpipeline1:amd64 (1.4.1-2) ...
Setting up man-db (2.7.5-1) ...
Building database of manual pages ...
Processing triggers for libc-bin (2.23-0ubuntu3) ...
#
```

## apt-get clean

I am in the process of cleaning up my system. And I see a lot of space occupied by this folder /var/cache/apt/archives (1.5GB).  
Is it absolutely necessary to keep all these archives?

```
root@raspberrypi:/var/cache/apt/archives# du -h
4.0K	./partial .
858M	.

root@raspberrypi:/var/cache/apt/archives#  apt-get clean

root@raspberrypi:/var/cache/apt/archives# du -h .
4.0K	./partial
96K 	.
root@raspberrypi:/var/cache/apt/archives#
```


## apt-get autoclean 

Seems like `sudo apt-get autoclean` is a better choice than sudo apt-get clean _autoclean will only remove "useless" archives.


## apt-key list

```bash
jacques@raspi5 [~]# apt-key list
/etc/apt/trusted.gpg
--------------------
pub   rsa2048 2012-04-01 [SC]
      A0DA 38D0 D76E 8B5D 6388  7281 9165 938D 90FD DD2E
uid           [ unknown] Mike Thompson (Raspberry Pi Debian armhf ARMv6+VFP) <mpthompson@gmail.com>
sub   rsa2048 2012-04-01 [E]

pub   rsa2048 2012-06-17 [SC]
      CF8A 1AF5 02A2 AA2D 763B  AE7E 82B1 2992 7FA3 303E
uid           [ unknown] Raspberry Pi Archive Signing Key
sub   rsa2048 2012-06-17 [E]

jacques@raspi5 [~]#
```
