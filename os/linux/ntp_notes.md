# Network Time Protocol - My Notes

## Correct Time Synchronization on Raspbian

```bash
root@raspi4:/etc/bind # date
Thu Jan 31 12:04:26 EST 2019

root@raspi4:/etc/bind # timedatectl 
      Local time: Thu 2019-01-31 12:04:41 EST
  Universal time: Thu 2019-01-31 17:04:41 UTC
        RTC time: n/a
       Time zone: America/Montreal (EST, -0500)
     NTP enabled: no
NTP synchronized: yes
 RTC in local TZ: no
      DST active: no
 Last DST change: DST ended at
                  Sun 2018-11-04 01:59:59 EDT
                  Sun 2018-11-04 01:00:00 EST
 Next DST change: DST begins (the clock jumps one hour forward) at
                  Sun 2019-03-10 01:59:59 EST
                  Sun 2019-03-10 03:00:00 EDT

root@raspi4:/etc/bind # timedatectl set-ntp True

root@raspi4:/etc/bind # timedatectl 
      Local time: Thu 2019-01-31 12:57:26 EST
  Universal time: Thu 2019-01-31 17:57:26 UTC
        RTC time: n/a
       Time zone: America/Montreal (EST, -0500)
     NTP enabled: yes
NTP synchronized: no
 RTC in local TZ: no
      DST active: no
 Last DST change: DST ended at
                  Sun 2018-11-04 01:59:59 EDT
                  Sun 2018-11-04 01:00:00 EST
 Next DST change: DST begins (the clock jumps one hour forward) at
                  Sun 2019-03-10 01:59:59 EST
                  Sun 2019-03-10 03:00:00 EDT

root@raspi4:/etc/bind # date
Thu Jan 31 12:57:40 EST 2019
root@raspi4:/etc/bind # 
```
