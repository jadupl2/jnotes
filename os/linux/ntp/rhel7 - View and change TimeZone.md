
LIST ALL TIME ZONES AVAILABLE 
====================================================================================================
root@sadmin~# timedatectl --no-pager list-timezones | grep -i montreal
root@sadmin~# timedatectl --no-pager list-timezones| grep -i toronto
America/Toronto
root@sadmin~#


LIST WHAT CURRENTLY USING 
====================================================================================================
root@sadmin~# timedatectl status
      Local time: Fri 2015-10-30 10:38:32 EDT
  Universal time: Fri 2015-10-30 14:38:32 UTC
        RTC time: Fri 2015-10-30 14:38:32
        Timezone: America/New_York (EDT, -0400)    <<<<=======
     NTP enabled: yes
NTP synchronized: yes
 RTC in local TZ: no
      DST active: yes
 Last DST change: DST began at
                  Sun 2015-03-08 01:59:59 EST
                  Sun 2015-03-08 03:00:00 EDT
 Next DST change: DST ends (the clock jumps one hour backwards) at
                  Sun 2015-11-01 01:59:59 EDT
                  Sun 2015-11-01 01:00:00 EST


CHANGE TIMEZONE 
====================================================================================================
root@sadmin~# timedatectl set-timezone America/Toronto


LIST WHAT CURRENTLY USING NOW
====================================================================================================root@sadmin~# timedatectl status
      Local time: Fri 2015-10-30 10:39:04 EDT
  Universal time: Fri 2015-10-30 14:39:04 UTC
        RTC time: Fri 2015-10-30 14:39:04
        Timezone: America/Toronto (EDT, -0400)
     NTP enabled: yes
NTP synchronized: yes
 RTC in local TZ: no
      DST active: yes
 Last DST change: DST began at
                  Sun 2015-03-08 01:59:59 EST
                  Sun 2015-03-08 03:00:00 EDT
 Next DST change: DST ends (the clock jumps one hour backwards) at
                  Sun 2015-11-01 01:59:59 EDT
                  Sun 2015-11-01 01:00:00 EST
root@sadmin~#
