http://www.meinberg.de/english/info/ntp.htm

root@gumby:~# cat /etc/ntp.conf
# Use public servers from the pool.ntp.org project.
# Please consider joining the pool (http://www.pool.ntp.org/join.html).
# http://www.pool.ntp.org/zone/ca
server 0.ca.pool.ntp.org
server 1.ca.pool.ntp.org
server 2.ca.pool.ntp.org
server 3.ca.pool.ntp.org


# Restrict Access given to these servers
restrict 0.ca.pool.ntp.org mask 255.255.255.255 nomodify notrap noquery
restrict 1.ca.pool.ntp.org mask 255.255.255.255 nomodify notrap noquery
restrict 2.ca.pool.ntp.org mask 255.255.255.255 nomodify notrap noquery
restrict 3.ca.pool.ntp.org mask 255.255.255.255 nomodify notrap noquery

# Permit all access over the loopback interface. 
restrict 127.0.0.1 

# Hosts on local network are less restricted (Can Query)
restrict 192.168.1.0 mask 255.255.255.0 nomodify notrap

# Undisciplined Local Clock. This is a fake driver intended for backup
# and when no outside source of synchronized time is available. 
fudge	127.127.1.0 stratum 10	
Additionally, there should be an entry for the local clock which can be used as a fallback resource if no other time source is available. Since the local clock is not very accurate, it should be fudged to a low stratum:

        server 127.127.1.0            # local clock
        fudge 127.127.1.0 stratum 12
        
# Drift file.  Put this in a directory which the daemon can write to.
driftfile /var/lib/ntp/drift
root@gumby:~# 




root@gumby:~# date
Wed Jul 22 19:05:43 EDT 2009
root@gumby:~# date 072018002009.09
Mon Jul 20 18:00:09 EDT 2009
root@gumby:~# ntpdate -u 0.ca.pool.ntp.org
22 Jul 19:06:33 ntpdate[26811]: step time server 24.215.0.24 offset 176757.319094 sec
root@gumby:~# ntpdate -u 0.ca.pool.ntp.org
22 Jul 19:06:37 ntpdate[26812]: adjust time server 24.215.0.24 offset 0.001216 sec
root@gumby:~# ntpdate -u 0.ca.pool.ntp.org
22 Jul 19:06:40 ntpdate[26813]: adjust time server 24.215.0.24 offset -0.001146 sec
root@gumby:~# date
Wed Jul 22 19:06:42 EDT 2009
root@gumby:~# 

root@gumby:~# ntpdate -q ca.pool.ntp.org
server 216.221.88.81, stratum 3, offset 0.000985, delay 0.05374
server 209.139.209.82, stratum 2, offset 0.006483, delay 0.09497
server 129.128.5.210, stratum 1, offset 0.003836, delay 0.10590
server 66.254.57.165, stratum 2, offset -0.005582, delay 0.06929
server 72.0.206.121, stratum 2, offset -0.119936, delay 0.37189
22 Jul 19:08:51 ntpdate[26836]: adjust time server 129.128.5.210 offset 0.003836 sec
root@gumby:~# 



root@gumby:~# ntpdate -q 0.ca.pool.ntp.org
server 199.85.124.148, stratum 3, offset -0.006311, delay 0.04573
server 208.80.96.70, stratum 3, offset 0.030502, delay 0.05507
server 174.142.75.135, stratum 3, offset 0.020665, delay 0.04356
server 24.215.0.24, stratum 2, offset 0.005395, delay 0.08522
server 66.96.30.35, stratum 2, offset 0.022107, delay 0.10722
22 Jul 19:12:44 ntpdate[26849]: adjust time server 24.215.0.24 offset 0.005395 sec

root@gumby:~# ntpdate -q 1.ca.pool.ntp.org
server 206.248.172.208, stratum 2, offset 0.028629, delay 0.05290
server 209.167.68.100, stratum 2, offset 0.036347, delay 0.04311
server 142.201.7.148, stratum 3, offset -0.002638, delay 0.04276
server 67.212.67.76, stratum 2, offset 0.039707, delay 0.05128
server 72.55.146.217, stratum 2, offset -0.006493, delay 0.03929
22 Jul 19:13:18 ntpdate[26850]: adjust time server 209.167.68.100 offset 0.036347 sec
root@gumby:~# 

root@gumby:~# ntpq -p
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
-ntp1.sscgateway 199.212.17.21    3 u   54   64   77   19.283  -31.946  24.480
*teardrop.ca     209.87.233.50    2 u   49   64   77   28.494    4.444   7.121
+ppp-7l.cyberabi 132.246.168.164  3 u   53   64   77   28.412   -2.630   8.127
+192.135.48.21   128.138.140.44   2 u   46   64   77   27.985    1.261   8.421
root@gumby:~# 
http://perdues.com/doc/ntp.html
The labeled columns for this are:

remote
    The IP address or DNS name of the remote server 
refid
    An identification of the type of the reference clock. 
st
    The "stratum" or level of the server: for almost all systems, 2 is great. Your local system will have a higher number. 
t
    The type of service. Your setup will show "l" for local on your local system, or "u" for "unicast" for communicating with remote servers. 
when
    This is the number of seconds since the server was last heard from. After a couple of minutes of operation your server should start to report numeric values here. 
poll
    Current polling interval in seconds. When remote servers are responding, "when" should be no greater than "poll". 
reach
    This and the remaining fields are important indicators of the health of your local server, your remote servers, and their communication. This field is really a bit array indicating whether responses have been received to your local server's eight most recent requests. The value starts at 0. If your local server is receiving responses to all its requests, it will go to 1, then 3, then 7. The display is in octal, so 377 is the maximum value. Anything less indicates that either your local server recently started or some requests did not receive responses. 
delay
    Recent average roundtrip time in milliseconds from request to response. 
offset
    Estimated differential between your system clock and this time server's clock, in milliseconds. You may consider this the "bottom line" on the accuracy of your system clock. NTP can usually drive this down to the level of the jitter or less. If your clock gets ahead or behind by more than 
jitter
    A measure of the variability of the delays between request and receipt of a response, in milliseconds. High jitter tends to limit your server's ability to synchronize accurately. 

=================================================================================
client test

[root@nano ~]# 
[root@nano ~]# date
Wed Jul 22 18:16:16 EDT 2009
[root@nano ~]# date 072012032009.09
Mon Jul 20 12:03:09 EDT 2009
[root@nano ~]# ntpdate -u 192.168.1.101
22 Jul 18:16:31 ntpdate[23200]: step time server 192.168.1.101 offset 195196.342866 sec
[root@nano ~]# ntpdate -u 192.168.1.101
22 Jul 18:16:33 ntpdate[23229]: adjust time server 192.168.1.101 offset -0.000472 sec
[root@nano ~]# date
Wed Jul 22 18:16:38 EDT 2009
[root@nano ~]# 


[root@nano ~]# 
[root@nano ~]# ntpdate -q 192.168.1.101
server 192.168.1.101, stratum 3, offset 0.001152, delay 0.02742
22 Jul 18:19:32 ntpdate[26331]: adjust time server 192.168.1.101 offset 0.001152 sec
[root@nano ~]# 
[root@nano ~]# ntpq -p 192.168.1.101
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
-clock.trit.net  192.12.19.20     2 u  123  128  377   77.286   12.135   0.390
+repos.lax-noc.c 204.123.2.5      2 u   49  256  377   77.264    3.106   1.452
*198.186.191.229 199.165.76.11    2 u  106  128  377   87.955   -0.463   1.153
+14.1e.5546.stat 198.72.72.10     3 u    5  256  377   57.385    1.279   1.124
[root@nano ~]# 


[root@nano ~]# ntpq -p
     remote           refid      st t when poll reach   delay   offset  jitter
==============================================================================
*gumby.maison.ca 206.248.172.208  3 u   64  128  217    0.833  -15.023  19.909
 LOCAL(0)        .LOCL.           5 l   60   64  377    0.000    0.000   0.002
[root@nano ~]# 

