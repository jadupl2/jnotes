# CentOS/Redhat BIND in chroot environment

## This article describes:

- How BIND can be run in a chroot environment on Red Hat Enterprise Linux 7
- What is the difference compared to Red Hat Enterprise Linux 6
- Recommendations when running BIND in a chroot environment
- Recommendations when editing configuration of BIND running in a chroot environment

## Running BIND in a chroot environment on RHEL 7 

To install BIND running in a chroot environment, you have to install the bind-chroot package 
(note that if you need to run BIND with SDB in a chroot, you need to install the bind-sdb-chroot 
package and substitute 'named-chroot' with 'named-sdb-chroot' in all of the following commands). 
You can do it by running the following command as root:

`# yum install bind-chroot`

To enable the named-chroot service, first check if the named service is running by issuing the following command:

`$ systemctl status named`

If it is running, it must be disabled. To disable named, issue the following commands as root:

`# systemctl stop named`

`# systemctl disable named`

Then, to enable the named-chroot service, issue the following command as root:

`# systemctl enable named-chroot`

To start the named-chroot service, issue the following command as root:

`# systemctl start named-chroot`

To check the status of the named-chroot service, issue the following command as root:

`# systemctl status named-chroot`

## What is the difference compared to Red Hat Enterprise Linux 6

### Red Hat Enterprise Linux 6

On Red Hat Enterprise Linux 6 the installation of bind-chroot package changed the environment variable ROOTDIR in /etc/sysconfig/named to point to the chroot environment location. After the installation start the named service by using the following command:

If you decided NOT to run the named service in the chroot environment you would have to remove the bind-chroot package or edit the ROOTDIR environment variable in /etc/sysconfig/named file manually.

### Red Hat Enterprise Linux 7

On Red Hat Enterprise Linux 7 the installation of bind-chroot does NOT change how the named service is run. On the contrary it installs new named-chroot service that needs to be started using systemctl command, if you want to run named service in a chroot environment.
To start named service in a chroot environment on Red Hat Enterprise Linux 7 issue the following command. For detailed information, please see the first paragraph of this article:

`# systemctl start named-chroot`

If you decided NOT to run the named service in the chroot environment you just need to start the normal named service by issuing the following command:

`# systemctl start named` 

## NOTES:
1. You don't need to remove the bind-chroot package before, as you need to on Red Hat Enterprise Linux 6.
2. named-chroot and normal named services can NOT be running at the same time. Please make sure to stop the running one before starting the other one.

## Recommendations when running BIND in a chroot environment

If you have installed the bind-chroot package, the BIND service will run in the chroot environment. In that case, the initialization script will mount all of the BIND configuration files into the chroot location using the mount --bind command, so that you can manage the configuration outside this environment. There is no need to copy anything into the /var/named/chroot/ directory because it is mounted automatically. This simplifies maintenance since you do not need to take any special care of BIND configuration files if it is run in a chroot environment. You can organize everything as you would with BIND not running in a chroot environment.  
The following directories are automatically mounted into /var/named/chroot/ if they are empty in the /var/named/chroot/ directory.  

They must be kept empty if you want them to be mounted into chroot environment:  
```
/var/named/
/run/named/ (only on Red Hat Enterprise Linux 7)
/etc/pki/dnssec-keys/
/etc/named/
/usr/lib64/bind/ or /usr/lib/bind/ (architecture dependent)
```

The following files are also mounted if the target file do not exist in /var/named/chroot/.
```
/etc/named.conf
/etc/rndc.conf
/etc/rndc.key
/etc/named.rfc1912.zones
/etc/named.dnssec.keys
/etc/named.iscdlv.key
/etc/named.root.key 
```

## Recommendations when editing configuration of BIND running in a chroot environment

Editing files which have been mounted in a chroot environment requires creating a backup copy and then editing the original file. Alternatively, use an editor with “edit-a-copy” mode disabled. For example, to edit the BIND's configuration file, /etc/named.conf, with Vim while it is running in a chroot environment, issue the following command as root:

`# vim -c "set backupcopy=yes" /etc/named.conf`


