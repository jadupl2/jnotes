## Notes AutoFS (Example with cifs) v1.0


### Require packages

```bash
[root@host]#  yum install samba-client samba-common cifs-utils [autofs]
```



### Change timeout before demounting mount point

```bash
root@holmes:/$ cat /etc/sysconfig/autofs 
#
# Init system options
#
# If the kernel supports using the autofs miscellanous device
# and you wish to use it you must set this configuration option
# to "yes" otherwise it will not be used.
#
USE_MISC_DEVICE="yes"
#
# Use OPTIONS to add automount(8) command line options that
# will be used when the daemon is started.
#
#OPTIONS="--timeout=900 --debug "
OPTIONS="--timeout=600 "       <=====
#
root@holmes:/$ 
```



### Create directories that will used as mount point

```bash
root@holmes:/home/jacques/Sherlock$ mkdir -p Sherlock/software
root@holmes:/home/jacques/Sherlock$ mkdir -p Sherlock/reference

root@holmes:/home/jacques$ ls -ld Sherlock/
drwxrwxr-x 4 root root 37 Jul 30 10:36 Sherlock/

root@holmes:/$ chown -R jacques:sadmin /Sherlock
root@holmes:/$ ls -l Sherlock/
total 0
drwxrwxr-x 2 jacques sadmin 6 Jul 30 10:36 reference
drwxrwxr-x 2 jacques sadmin 6 Jul 30 10:36 software
root@holmes:/$ 
```



### Insert the Mount Point for all your Windows Share

```bash
root@holmes:~$ nl /etc/auto.master
     1	#
     2	# Sample auto.master file
     3	# This is a 'master' automounter map and it has the following format:
     4	# mount-point [map-type[,format]:]map [options]
     5	# For details of the format look at auto.master(5).
     6	#
     7	#/misc		/etc/auto.misc
     8	/home/jacques/Sherlock		/etc/auto.sherlock   <======
     9	#
    10	# NOTE: mounts done from a hosts map will be mounted with the
    11	#	"nosuid" and "nodev" options unless the "suid" and "dev"
    12	#	options are explicitly given.
    13	#
    14	/net	-hosts
    15	#
    16	# Include /etc/auto.master.d/*.autofs
    17	# The included files must conform to the format of this file.
    18	#
    19	+dir:/etc/auto.master.d
    20	#
    21	# Include central master map if it can be found using
    22	# nsswitch sources.
    23	#
    24	# Note that if there are entries for /net or /misc (as
    25	# above) in the included master map any keys that are the
    26	# same will not be seen as the first read key seen takes
    27	# precedence.
    28	#
    29	+auto.master
root@holmes:~$ 
```



### Create the map file for the two shares

```bash
root@holmes:~$ cat /etc/auto.sherlock 
reference -fstype=cifs,rw,noperm,credentials=/home/jacques/.smb.txt  ://192.168.1.48/Reference
software  -fstype=cifs,rw,noperm,credentials=/home/jacques/.smb.txt  ://192.168.1.48/Software
root@holmes:~$ 
```



### Create the windows account use to access the shares

```bash
root@holmes:/etc$ vi /home/jacques/.smb.txt 
username=batman
password=batPassword
domain=maison

root@holmes:/etc$ chmod 600 /home/jacques/.smb.txt 
root@holmes:/etc$ chown jacques:sadmin /home/jacques/.smb.txt 

root@holmes:~$ ls -l /home/jacques/.smb.txt
-rw------- 1 jacques sadmin 50 Jul 30 16:41 /home/jacques/.smb.txt
root@holmes:~$ 
```



### Enable and start Automount Service

```bash
root@holmes:/etc$ systemctl enable --now autofs
Created symlink from /etc/systemd/system/multi-user.target.wants/autofs.service to /usr/lib/systemd/system/autofs.service.

root@holmes:/etc$ systemctl status autofs
● autofs.service - Automounts filesystems on demand
   Loaded: loaded (/usr/lib/systemd/system/autofs.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2019-07-30 10:25:09 EDT; 56min ago
 Main PID: 31873 (automount)
   CGroup: /system.slice/autofs.service
           └─31873 /usr/sbin/automount --timeout=900 --debug --foreground --dont-check-daemon

Jul 30 11:21:03 holmes.maison.ca automount[31873]: st_expire: state 1 path /home/jacques/Sherlock
Jul 30 11:21:03 holmes.maison.ca automount[31873]: expire_proc: exp_proc = 140522377418496 path /home/jacques/Sherlock
Jul 30 11:21:03 holmes.maison.ca automount[31873]: expire_cleanup: got thid 140522377418496 path /home/jacques/Sherlock stat 0
Jul 30 11:21:03 holmes.maison.ca automount[31873]: expire_cleanup: sigchld: exp 140522377418496 finished, switching from 2 to 1
Jul 30 11:21:03 holmes.maison.ca automount[31873]: st_ready: st_ready(): state = 2 path /home/jacques/Sherlock
Jul 30 11:21:41 holmes.maison.ca automount[31873]: st_expire: state 1 path /net
Jul 30 11:21:41 holmes.maison.ca automount[31873]: expire_proc: exp_proc = 140522377418496 path /net
Jul 30 11:21:41 holmes.maison.ca automount[31873]: expire_cleanup: got thid 140522377418496 path /net stat 0
Jul 30 11:21:41 holmes.maison.ca automount[31873]: expire_cleanup: sigchld: exp 140522377418496 finished, switching from 2 to 1
Jul 30 11:21:41 holmes.maison.ca automount[31873]: st_ready: st_ready(): state = 2 path /net
root@holmes:/etc$ 
```



### Test Automount (Work with one mount point , but not with the second ?)

```bash
root@holmes:~$ df -h | grep -i sherlock
root@holmes:~$ mount | grep -i sherlock
/etc/auto.sherlock on /home/jacques/Sherlock type autofs (rw,relatime,fd=5,pgrp=23756,timeout=300,minproto=5,maxproto=5,indirect,pipe_ino=1179201)
root@holmes:~$ ls -l /home/jacques/Sherlock/reference
total 144
-rwxr-xr-x 1 root root 145977 Apr 13 15:17 2019 How to Type the Escape Key on iPad Keyboard.pdf
drwxr-xr-x 2 root root      0 Jul 26 21:24 ALire
drwxr-xr-x 2 root root      0 Mar 29 00:28 Calibre_Library

root@holmes:~$ df -h | grep -i sherlock
//192.168.1.48/Reference      932G  496G  436G  54% /home/jacques/Sherlock/reference
root@holmes:~$ mount | grep -i sherlock
/etc/auto.sherlock on /home/jacques/Sherlock type autofs (rw,relatime,fd=5,pgrp=23756,timeout=300,minproto=5,maxproto=5,indirect,pipe_ino=1179201)
//192.168.1.48/Reference on /home/jacques/Sherlock/reference type cifs (rw,relatime,vers=default,cache=strict,username=jacques,domain=maison,uid=0,noforceuid,gid=0,noforcegid,addr=192.168.1.48,file_mode=0755,dir_mode=0755,soft,nounix,serverino,mapposix,noperm,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1)

root@holmes:~$ ls -l /home/jacques/Sherlock/software
total 1
drwxr-xr-x 2 root root   0 Feb 15 20:48 Development
-rwxr-xr-x 1 root root 197 Dec 30  2018 Readme_USB.txt
drwxr-xr-x 2 root root   0 Jul 21 07:35 Software_Home_Systems
drwxr-xr-x 2 root root   0 Oct  1  2017 Software_ISO
drwxr-xr-x 2 root root   0 Jul 27 11:30 Software_Linux
drwxr-xr-x 2 root root   0 Jul 28 09:34 Software_OSX
drwxr-xr-x 2 root root   0 Apr 15 18:30 Software_VMware_ESX
drwxr-xr-x 2 root root   0 Jul 28 09:35 Software_Windows

root@holmes:~$ mount | grep -i sherlock
/etc/auto.sherlock on /home/jacques/Sherlock type autofs (rw,relatime,fd=5,pgrp=23756,timeout=300,minproto=5,maxproto=5,indirect,pipe_ino=1179201)
//192.168.1.48/Reference on /home/jacques/Sherlock/reference type cifs (rw,relatime,vers=default,cache=strict,username=jacques,domain=maison,uid=0,noforceuid,gid=0,noforcegid,addr=192.168.1.48,file_mode=0755,dir_mode=0755,soft,nounix,serverino,mapposix,noperm,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1)
//192.168.1.48/Software on /home/jacques/Sherlock/software type cifs (rw,relatime,vers=default,cache=strict,username=jacques,domain=maison,uid=0,noforceuid,gid=0,noforcegid,addr=192.168.1.48,file_mode=0755,dir_mode=0755,soft,nounix,serverino,mapposix,noperm,rsize=1048576,wsize=1048576,echo_interval=60,actimeo=1)

root@holmes:~$ df -h | grep -i sherlock
//192.168.1.48/Reference      932G  496G  436G  54% /home/jacques/Sherlock/reference
//192.168.1.48/Software       932G  496G  436G  54% /home/jacques/Sherlock/software
root@holmes:~$ 
```



### Debugging Autofs with journalctl

```bash
root@holmes:/$ cat /etc/sysconfig/autofs 
#
# Init syatem options
#
# If the kernel supports using the autofs miscellanous device
# and you wish to use it you must set this configuration option
# to "yes" otherwise it will not be used.
#
USE_MISC_DEVICE="yes"
#
# Use OPTIONS to add automount(8) command line options that
# will be used when the daemon is started.
#
OPTIONS="--timeout=900 --debug " <<======
#
root@holmes:/$ 
```

```bash
root@holmes:/$ journalctl -u autofs -f
-- Logs begin at Tue 2019-07-09 17:23:21 EDT. --
Jul 30 11:55:15 holmes.maison.ca automount[25300]: expire_proc_indirect: expire /sherlock/reference
Jul 30 11:55:15 holmes.maison.ca automount[25300]: 1 remaining in /sherlock
Jul 30 11:55:15 holmes.maison.ca automount[25300]: expire_cleanup: got thid 140002850150144 path /sherlock stat 3
Jul 30 11:55:15 holmes.maison.ca automount[25300]: expire_cleanup: sigchld: exp 140002850150144 finished, switching from 2 to 1
```

