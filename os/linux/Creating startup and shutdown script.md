# How do I enable a script to start and stop a service

For a script to interact with chkconfig and the automatic starting and stopping of services at 
different run levels, it needs two key components.

The first is a commented section that allows the script to be managed by the chkconfig tool. Here is an example:

```bash
#!/bin/bash
#
# chkconfig 345 10 90
# description This is where you put a description of your service
```

In this example, the numbers 345 after the chkconfig directive represent the runlevels that the service will be enabled for by default. In this example the service will be enabled by default for runlevels 3, 4, and 5.

The 10 is the start priority. The lower the number the higher the priority and the sooner a service will be started. The 90 is the stop priority. The lower the number the higher the priority and the sooner a service will be stopped when changing runlevels.


The second essential component is that the script must support being called with the arguments "start" and "stop." The script will be called with "start" as an argument when the service is being started at a given runlevel and "stop" when it is being stopped. Within these functions the script should touch a lock file in the /var/lock/subsys/ folder. If this is not done, the script will automatically start, but will not stop automatically. This should be done in the following manner:

```bash
start() {
  ...
  touch /var/lock/subsys/servicename

}

stop() {
  ...
  rm -f /var/lock/subsys/servicename
}
```

Once you have created a script that conforms to these two requirements, place a copy of it into the /etc/init.d/ directory and execute the command:

`chkconfig --add servicename`

This will register the service and create the necessary links in the appropriate rcX.d directories. You can now administer this service using chkconfig command.
How do I enable a script to start and stop a service at different  runlevels using chkconfig?
by Joshua Wulf
For a script to interact with chkconfig and the automatic starting and stopping of services at different run levels, it needs two key components.
The first is a commented section that allows the script to be managed by the chkconfig tool. Here is an example:
#!/bin/bash


#
# chkconfig 345 10 90
# description This is where you put a description of your service
In this example, the numbers 345 after the chkconfig directive represent the runlevels that the service will be enabled for by default. In this example the service will be enabled by default for runlevels 3, 4, and 5.
The 10 is the start priority. The lower the number the higher the priority and the sooner a service will be started. The 90 is the stop priority. The lower the number the higher the priority and the sooner a service will be stopped when changing runlevels.
The second essential component is that the script must support being called with the arguments "start" and "stop." The script will be called with "start" as an argument when the service is being started at a given runlevel and "stop" when it is being stopped. Within these functions the script should touch a lock file in the /var/lock/subsys/ folder. If this is not done, the script will automatically start, but will not stop automatically. This should be done in the following manner:
start() {
  ...
  touch /var/lock/subsys/servicename


}


stop() {
  ...
  rm -f /var/lock/subsys/servicename
}
Once you have created a script that conforms to these two requirements, place a copy of it into the /etc/init.d/ directory and execute the command:
chkconfig --add servicename
This will register the service and create the necessary links in the appropriate rcX.d directories. You can now administer this service using chkconfig command.

## Example to activate and deactivate a startup/shutdown script

```bash
root@debian7/etc/init.d# cat sadmin
#!/bin/sh
#
# chkconfig: 345 72 19
# description: SADMIN init script
#
# --------------------------------------------------------------------------------------------------
# SysV SADMIN Service Definition 
# If you modify this file, Run these commands to make your change effective
#   Enter   $SADMIN/bin/sadm_service_ctrl.sh -d     # Disable SADMIN Service
#           $SADMIN/bin/sadm_service_ctrl.sh -e     # Enable SADMIN Service
#           $SADMIN/bin/sadm_service_ctrl.sh -s     # Show Status of SADMIN Service
# --------------------------------------------------------------------------------------------------
# Description:       sadmin startup and shutdown script
#
# --------------------------------------------------------------------------------------------------
# V2.0 June 2017 - Update
# V2.1 July 2017 - Added LSB tags to correct warning on Debian 7
# V2.2 January 2018 - Run /etc/profile.d/sadmin.sh to make SADMIN Env. Variable Available to script
#@2019_03_27 Update: v2.3 Redirect output from startup/shutdown script to /dev/null (Already log).
#
# --------------------------------------------------------------------------------------------------
#
ACTION=$1                                       ; export ACTION         # start/stop/status/restart
SADM_HOSTNAME=`hostname -s`                     ; export HOSTNAME       # Current Host name 
export SADMIN=`grep "^SADMIN=" /etc/environment |awk -F= '{print $2}'`  # Get SADMIN Install Dir
if [ "$SADMIN" = "" ]                                                   # Couldn't get Install Dir.
    then printf "Couldn't get SADMIN variable in /etc/environment"      # Advise User What's wrong
         printf "SADMIN service script aborted."                        # Advise what to fix
         exit 1                                                         # Exit Script with error
fi
SADM_SYS_DIR="${SADMIN}/sys"                    ; export SADM_SYS_DIR   # Startup/Shutdown Dir.
SADM_START="${SADM_SYS_DIR}/sadm_startup.sh"    ; export SADM_START     # Startup Script
SADM_STOP="${SADM_SYS_DIR}/sadm_shutdown.sh"    ; export SADM_STOP      # Shutdown Script
USAGE="Usage: $0 start|stop|restart|status\n"   ; export USAGE          # Script Usage 

# Create SADMIN service lock file
if [ -d /var/lock/subsys ] ; then SADM_LOCKFILE=/var/lock/subsys/sadmin ; fi 

# If startup script is not found - exit script
if [ ! -f "$SADM_START" ] ; then printf "The script $SADM_START cannot be found" ; exit 1 ; fi

# If shutdown script is not found - exit script
if [ ! -f "$SADM_STOP"  ] ; then printf "The script $SADM_STOP cannot be found"  ; exit 1 ; fi

# Make sure the script are executable and only by root 
chmod 700 ${SADM_START} ; chmod 700 ${SADM_STOP}

# depending on parameter -- startup, shutdown, restart or usage display
case "$ACTION" in
    "start")    printf "Starting sadmin : "
                $SADM_START >/dev/null 2>&1
                touch $SADM_LOCKFILE 
                printf "[OK]\n"
                ;;
    "stop")     printf "Shutdown sadmin : "
                $SADM_STOP >/dev/null 2>&1
                rm -f $SADM_LOCKFILE >/dev/null 2>&1
                printf "[OK]\n"
                ;;
    "restart")  $0 stop
                $0 start
                ;;
    "status")   chkconfig --list | grep sadmin
                ;;
    *)          printf "${USAGE}\n" 
                exit 1
                ;;
esac
exit 0
root@debian7/etc/init.d# 
```


## Activate startup/shutdown script on SysV system

```bash
root@debian7/etc/init.d# sadm_service_ctrl.sh -e (ENABLE)
Checking availability of command chkconfig ... /sbin/chkconfig [OK]
cp /sadmin/cfg/.sadmin.rc /etc/init.d/sadmin ... [OK]
chmod 755 /etc/init.d/sadmin ... [OK]
chown root:root /etc/init.d/sadmin ...[OK]
chkconfig -add sadmin ...[OK]
chkconfig sadmin on ...[OK]
service sadmin stop ...[OK]
service sadmin start ... [OK]
service sadmin status ... 
sadmin                    0:off  1:off  2:on   3:on   4:on   5:on   6:off
[OK]

Consequence of enabling SADMIN service :
/sadmin/sys/sadm_startup.sh, will be executed at system startup.
/sadmin/sys/sadm_shutdown.sh will be executed on system shutdown.
We encourage you to customize these two scripts to your need.

root@debian7/etc/init.d# 
```

## De-activate startup/shutdown script on SysV system

```bash
root@debian7/etc/init.d# sadm_service_ctrl.sh -d
Checking availability of command chkconfig ... /sbin/chkconfig [OK]
service sadmin stop ...[OK]
chkconfig sadmin off ... [OK]
chkconfig --del sadmin ... [OK]
Remove service file /etc/init.d/sadmin ... [OK]

Consequence of disabling SADMIN service :
/sadmin/sys/sadm_startup.sh, will not be executed at system startup.
/sadmin/sys/sadm_shutdown.sh will not be executed on system shutdown.

root@debian7/etc/init.d#
```



## De-activate startup/shutdown script on system using systemd

```bash
root@holmes:~$ sadm_service_ctrl.sh -d
systemctl stop sadmin.service ... [OK]
systemctl disable sadmin.service...[OK]
rm -f /etc/systemd/system/sadmin.service ...[OK]

Consequence of disabling SADMIN service :
/sadmin/sys/sadm_startup.sh, will not be executed at system startup.
/sadmin/sys/sadm_shutdown.sh will not be executed on system shutdown.

root@holmes:~$ 

```

## Activate startup/shutdown script on system using systemd

```bash
root@holmes:~$ sadm_service_ctrl.sh -e
cp /sadmin/cfg/.sadmin.service /etc/systemd/system/sadmin.service ... [OK]
systemctl enable sadmin.service ... [OK]
systemctl stop sadmin.service ... [OK]
systemctl start sadmin.service ... [OK]
systemctl status sadmin.service ... [OK]
sadmin.service                                enabled 

Consequence of enabling SADMIN service :
/sadmin/sys/sadm_startup.sh, will be executed at system startup.
/sadmin/sys/sadm_shutdown.sh will be executed on system shutdown.
We encourage you to customize these two scripts to your need.

root@holmes:~$ 
```
