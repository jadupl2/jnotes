# User Creation Notes


## User creation default values
```bash  
$ useradd -D  
GROUP=602  
HOME=/home  
INACTIVE=-1  
EXPIRE=  
SHELL=/bin/bash  
SKEL=/etc/skel  
CREATE_MAIL_SPOOL=yes  
root@holmes:/sadmin/bin$   

$ cat /etc/default/useradd
# useradd defaults file
HOME=/home
INACTIVE=-1
EXPIRE=
SHELL=/bin/bash
SKEL=/etc/skel
CREATE_MAIL_SPOOL=yes
GROUP=602
root@holmes:/etc/default$ 
```


