# User account lock mechanism

## Account is lock because it as no password.
---
### Verify account status - User 'sadmin' is locked
```bash
root@gotham:~# passwd -S sadmin
sadmin LK 2016-02-19 0 90 7 90 (Password locked.)
```

### Two '!!' means account has no password, we need to set one
```bash
root@gotham:~# grep sadmin /etc/shadow
sadmin:!!:16851:0:90:7:90::
```

### We need to assign a password to the 'sadmin' account
```bash
root@gotham:~# passwd sadmin
Changing password for user sadmin.
New password: 
Retype new password: 
passwd: all authentication tokens updated successfully.
```

### Now we can see that user is unlock 
```bash
root@gotham:~# passwd -S sadmin
sadmin PS 2018-11-23 0 90 7 90 (Password set, MD5 crypt.)
```

### The file /etc/shadow confirm it.
```bash
root@gotham:~# awk -F: '/^sadmin/' /etc/shadow
sadmin:$1$a8yvfRVz$6VziuaoB9fIEfLY8zwd7P/:17859:0:90:7:90::
```

---
## Account was lock with the 'passwd -l' command.
---
### Let's lock the account now.
```bash
root@gotham:~# passwd -l sadmin
Locking password for user sadmin.
passwd: Success
```

### Let's check /etc/shadow, We have two '!!' follow by the password hash
```bash
root@gotham:~# awk -F: '/^sadmin/' /etc/shadow
sadmin:!!$1$a8yvfRVz$6VziuaoB9fIEfLY8zwd7P/:17859:0:90:7:90::
```

### And the 'passwd -S' confirm that it is lock
```bash
root@gotham:~# passwd -S sadmin
sadmin LK 2018-11-23 0 90 7 90 (Password locked.)
```

### Now unlock the user
```bash
root@gotham:~# passwd -u sadmin
Unlocking password for user sadmin.
passwd: Success
```

### Check if the two !! are removed (Yes there are)
```bash
root@gotham:~# awk -F: '/^sadmin/' /etc/shadow 
sadmin:$1$a8yvfRVz$6VziuaoB9fIEfLY8zwd7P/:17859:0:90:7:90::
```

### Check the user status - ok not lock anymore
```bash
root@gotham:~# passwd -S sadmin
sadmin PS 2018-11-23 0 90 7 90 (Password set, MD5 crypt.)
```
---
## Lock user account with 'usermod -L' 
---
### Check account status
```bash
root@gotham:~# passwd -S sadmin
sadmin PS 2018-11-23 0 90 7 90 (Password set, MD5 crypt.)
```

### Show user /etc/shadow line
```bash
root@gotham:~# awk -F: '/^sadmin/' /etc/shadow 
sadmin:$1$a8yvfRVz$6VziuaoB9fIEfLY8zwd7P/:17859:0:90:7:90::
```

### Lock the user now
```bash
root@gotham:~# usermod -L sadmin
```
### The status indicate that it is lock now
```bash
root@gotham:~# passwd -S sadmin
sadmin LK 2018-11-23 0 90 7 90 (Password locked.)
```

### Show user /etc/shadow line after locking the account with usermod
```bash
root@gotham:~# awk -F: '/^sadmin/' /etc/shadow 
sadmin:!$1$a8yvfRVz$6VziuaoB9fIEfLY8zwd7P/:17859:0:90:7:90::
```


### Now let's unlock it with usermod
```bash
root@gotham:~# usermod -U sadmin
```

### The '!' is now remove from the password hash (not lock anymore)
```bash
root@gotham:~# awk -F: '/^sadmin/' /etc/shadow 
sadmin:$1$a8yvfRVz$6VziuaoB9fIEfLY8zwd7P/:17859:0:90:7:90::
```

### Check status of the account
```bash
root@gotham:~# passwd -S sadmin
sadmin PS 2018-11-23 0 90 7 90 (Password set, MD5 crypt.)
```
