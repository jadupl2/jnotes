## Inits in Fedora and RHEL
- SysVinit
    - Fedora < 9
    - Red Hat Enterprise Linux < 6.0
- upstart
    - Fedora 9 – 14
    - Red Hat Enterprise Linux 6
- systemd
    - Fedora 15 (Rawhide)



## SysVinit – basic deployment
- /etc/inittab 
- system boot
```bash
      si::sysinit:/etc/rc.d/rc.sysinit
      id:3:initdefault:
      l3:3:wait:/etc/rc.d/rc 3
```
- ttys
```bash
    1:2345:respawn:/sbin/mingetty tty1
    x:5:respawn:/etc/X11/prefdm -nodaemon
```
- control-alt-delete
```bash
      ca::ctrlaltdel:/sbin/shutdown -t3 -r now
```
- runlevel change
    - telinit <runlevel>



/etc/inittab system boot
      si::sysinit:/etc/rc.d/rc.sysinit
      id:3:initdefault:
      l3:3:wait:/etc/rc.d/rc 3
ttys
●
●
●
1:2345:respawn:/sbin/mingetty tty1
x:5:respawn:/etc/X11/prefdm -nodaemon
SysVinit – basic deployment
control-alt-delete
      ca::ctrlaltdel:/sbin/shutdown -t3 -r now
 runlevel change
telinit <runlevel>
 ● ●
/etc/init/ system boot
startup event /etc/init/rcS.conf
        /etc/rc.d/rc.sysinit
        exec telinit $runlevel
●
●
ttys
–
–
runlevel event
/etc/init/rc.conf -> /etc/rc.d/rc $RUNLEVEL
upstart – basic deployment
/etc/init/start-ttys.conf
   initctl start tty TTY=$tty
 control-alt-delete
/etc/init/control-alt-delete.conf

 ● ●
●
/etc/systemd/system, /lib/systemd/system system boot
   default-target -> multiuser.target wants basic.target
   wants sysinit.target
●
control-alt-delete
   ctrl-alt-del.target
systemd – basic deployment
ttys
getty.target -> getty.target.wants
getty@tty1.service -> /lib/systemd/system/getty@.service
 ●
Change target 
   systemctl isolate <target> 

 controlling services
●SysVinit
$ service <service> status
$ service status –all
# service <service> start|stop
●upstart
$ initctl status <job>
$ initctl list
# initctl start|stop <job>
●systemd
# systemctl
# systemctl list-units –all # systemctl status <unit>
# systemctl start|stop <unit>
 
 ●
SysVinit
●
upstart
   # cp <job>.conf /etc/init
Installing services edit <service> file
   # chkconfig: 2345 90 60
   # chkconfig: - 86 14
# cp <service> /etc/rc.d/init.d
# chkconfig add <service>
 
 ●
systemd
Install units edit <unit file>
   [Unit]
   After=syslog.target
   [Install]
   WantedBy=multi-user.target
# cp <unit> /lib/systemd/system
# systemctl enable <unit>
 
Enabling/disabling service
●SysVinit
# chkconfig [--level <levels>] <service> on
# chkconfig [--level <levels>] <service> off|reset
●upstart
add start on runlevel RUNLEVEL=[<levels>] to job file remove <job> file or since upstart-0.6.7 add manual stanza to job
●systemd
# ln -s /lib/systemd/system/<unit> /etc/systemd/system/<target>.wants
# rm /etc/systemd/system/<target>.wants/<unit> or systemctl disable <unit>
# systemctl daemon-reload
