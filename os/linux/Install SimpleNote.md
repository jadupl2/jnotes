## Install/Update SimpleNote on CentOS/Redhat

###Download latest version at 
https://github.com/Automattic/simplenote-electron/releases/

1. Change tgo the application directory
    ```bash
    # cd ~/Documents/Apps`

    /home/jacques/Documents/Apps# ls -l
    total 4
    lrwxrwxrwx 1 jacques sadmin   22 Apr 25 10:00 simplenote -> ./Simplenote-linux-x64
    drwxr-xr-x 4 jacques sadmin 4096 Feb  8  2018 Simplenote-linux-x64
    /home/jacques/Documents/Apps# 

    /home/jacques/Documents/Apps# ls -l /home/jacques/Downloads/Simplenote-1.1.7.tar.gz 
    -rw-rw-r-- 1 jacques sadmin 54282897 Aug 21 08:50 /home/jacques/Downloads/Simplenote-1.1.7.tar.gz
    /home/jacques/Documents/Apps# 
    ```
    
#### UNTAR NEW VERSION IN Apps DIRECTORY
/home/jacques/Documents/Apps# tar -xvzf /home/jacques/Downloads/Simplenote-1.1.7.tar.gz

REMOVE PREVIOUS LINK AND VERSION
~/Documents/Apps$ rm -fr Simplenote-linux-x64/
~/Documents/Apps$ rm simplenote
rm: remove symbolic link ‘simplenote’? y

CREATE NEW LINK
~/Documents/Apps$ ln -s ./simplenote-1.1.7/simplenote ./simplenote

END PICTURE
~/Documents/Apps$ ls -l
total 4
lrwxrwxrwx 1 jacques sadmin   29 Aug 21 09:43 simplenote -> ./simplenote-1.1.7/simplenote
drwxrwxrwx 4 root    root   4096 Aug 14 17:11 simplenote-1.1.7
jacques@holmes:~/Documents/Apps$ 