# Creating a self signed certificate

## Password will be asked when starting the http server

```bash
-bash-3.1# openssl genrsa -des3 -rand /bin/ksh:/bin/tcsh -out useradmin.maison.ca 1024
1524808 semi-random bytes loaded
Generating RSA private key, 1024 bit long modulus
..................................++++++
...................++++++
e is 65537 (0x10001)
Enter pass phrase for useradmin.maison.ca:
Verifying - Enter pass phrase for useradmin.maison.ca:
-bash-3.1#

-bash-3.1# cat useradmin.maison.ca
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,E45A57DCB85189DA

54mRdfTaFekFX5Rw0q7stYxdwwRLvGoZ2i1KKzUUMImYUjmJNooXgPkdXNqm4t/Y
3M7qQcTqkphe8GhJnqI2ipz2WnXWZkmTf4b/KwGizd3gc+xmIJo3eBdvHGGS3T55
XwbhBASvZ5UyErZIWwq/3wYL55kXA+QKMsSvxRpTxModmb3bvoh8aPU9+P43YMcs
fWt8dj1pbYVej0mlpoifmnQ76Gn0mR5NdsgjqJOJMjDYqXjZsS3fKKYMdxLjyY4N
MycP3n1JQFnWPHqd9nk44bIwdtkgOp2CnVyqAFgshk3IFHuo9A8dXu7MzMKztlO7
hW6+3Ogt4DkWJ7Qf6ogRKV81mM+Vl534erQM2KbBjyrWUQLliANGjMNTSC0vYs32
HpsxvKlxu3EfeUzutox3TdXHZsZzwhkvGRSWfapxgxOavX/j04g3zWroio/NKyQT
PPjefnxjOdhFIiRjiyRfZEdOv54QcBoKvWSxz4tqHWYjqGoNoGXLaGMcV2HIqVWd
W9P0+dMqQ9qGDofwkclsmDN4Bnep+dY6Gp4VIn6j6T6dzCR/jIMMnD/jpCF5qgpH
jAcTNIx+KnxMLrqfhH4cdZoDqF/xVIC/pIgbvoNR1GjnjF2inX3CX9df+g5vpaU4
Qj2OMA5T2ns7QOsoRJWf/Dn1ZoeIPzT5G5eihRagKpB4j+0posBVAEx+7m+gL2cU
9Yo+gkkRfUin1W0TuKZ8Y0rJlfijuzY2klvpR6gFoxay7s+f/TqRDBt4FXIpaQJk
6mVV9NLjqmmRPD9YY/vUdSsSe5OIb2lnlGCY/MRoucUgLZmxeiQmDQ==
-----END RSA PRIVATE KEY-----
-bash-3.1#
```

## Password will NOT be asked when starting the http server

```bash
ash-3.1# openssl genrsa  -rand /bin/ksh:/bin/tcsh -out useradmin.maison.ca 1024
1524808 semi-random bytes loaded
Generating RSA private key, 1024 bit long modulus
..........................++++++
..........................................++++++
e is 65537 (0x10001)
-bash-3.1#
-bash-3.1# cat useradmin.maison.ca
-----BEGIN RSA PRIVATE KEY-----
MIICXgIBAAKBgQDF1b40uafgznNIG6hyvMsFuojEbbTo+H2hWtBIrggLajhLtTvf
QW61RdaDOJiwHFclhhglyEvudQ2CjQgDLXhaUtHY+Lv9GDwnOt1PZ3ONB8JKj1yw
UXDzLvCIrpfvrNjl5irDVD3TX0Xsto+1oxv6uDxmUwuYsSjOqSCNQ20kuwIDAQAB
AoGBALeVcbxbzH3BFxmDEP3oVD4sq14csBTBA1QIgd/wH2b4WuVmePjkL2foNvfu
PUKwCe64URA+nrfZx3+ugBDy0Qrd9bNvPeMbOmCR10ALpFPDsLhUz/BU4r7+SSGv
169Urz7Edn9P4QSg5IfUcAj1Oo2QJnxQY0BP8hXriK74YeGBAkEA723ux0gqm+Nv
N8f37zVY+fvehYvkXE9Apra42DPVv7yHgv7s9g1mELI8e/WBVS44o1X8lfaNqDYa
yaJr7rx5bwJBANOG3Sx2gaBjyDEviH0kJCATWm+/7/hIvpVkcemuZH+sA2GIXIDw
1DX0kb+ne73IkZyG/fLUc2pCfQ9ADlVcK3UCQQDbNFm1R8QC3vH3LvQ2pC/nBwrO
V6pmPQTi3U1UW/ipR1EFxKFsEA5O4O/xSW70bpHaMKJFJerBg295BWQtuY6vAkEA
wdVI4dCqkmY9cc0D/uDadcYmTlcNOHC4rD9CH4geMdxXsM17LvEBFXxK/twkCVI9
FLmjBh1msc3BufLR76ipbQJAGFN8ihojKck7iaFXKejYFNHAlLAVnJPWpMcFZbmx
n69unRlVvovS/eoKkDkn8uxNgQIPSE83zM1nq01e4EKXMA==
-----END RSA PRIVATE KEY-----
-bash-3.1#
```

## Creating a Certificate Signing Request

```bash
-bash-3.1# openssl req -new -key useradmin.maison.ca -out useradmin.maison.ca.csr
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [GB]:CA
State or Province Name (full name) [Berkshire]:Quebec
Locality Name (eg, city) [Newbury]:Montreal
Organization Name (eg, company) [My Company Ltd]:Anemone
Organizational Unit Name (eg, section) []:Tech.Support
Common Name (eg, your name or your server's hostname) []:useradmin.maison.ca
Email Address []:jadupl2@gmail.com

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
-bash-3.1#

-bash-3.1# ls -l useradmin.maison*
-rw-r--r-- 1 root security 891 Jan 21 11:09 useradmin.maison.ca
-rw-r--r-- 1 root security 753 Jan 21 11:17 useradmin.maison.ca.csr
-bash-3.1#

-bash-3.1# cat useradmin.maison.ca.csr
-----BEGIN CERTIFICATE REQUEST-----
MIIB9DCCAV0CAQAwgZoxCzAJBgNVBAYTAkNBMQ8wDQYDVQQIEwZRdWViZWMxETAP
BgNVBAcTCE1vbnRyZWFsMRAwDgYDVQQKEwdBbmVtb25lMRUwEwYDVQQLEwxUZWNo
LlN1cHBvcnQxHDAaBgNVBAMTE3VzZXJhZG1pbi5tYWlzb24uY2ExIDAeBgkqhkiG
9w0BCQEWEWphZHVwbDJAZ21haWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCB
iQKBgQDF1b40uafgznNIG6hyvMsFuojEbbTo+H2hWtBIrggLajhLtTvfQW61RdaD
OJiwHFclhhglyEvudQ2CjQgDLXhaUtHY+Lv9GDwnOt1PZ3ONB8JKj1ywUXDzLvCI
rpfvrNjl5irDVD3TX0Xsto+1oxv6uDxmUwuYsSjOqSCNQ20kuwIDAQABoBkwFwYJ
KoZIhvcNAQkHMQoTCFNIMjIxYmJzMA0GCSqGSIb3DQEBBQUAA4GBAH0zTRY6bXz3
7x/TQG+C2RxADTNMpk1mIHF+l38LFnOOXsqf7BDfRcDYLqG2vMMM7HuQRRk2eOaN
6zm2GJtiL9wcEoDz/5NGxRIZve6HbBEuaumxDm0/EloFiO93PSwGgo3KV7KMvSLR
DY6JhTQD3nfPIji4wX3AceOIEGoya0gu
-----END CERTIFICATE REQUEST-----
-bash-3.1#
````

## Learn about the content of the certificate request

```bash
-bash-3.1# openssl req -noout -text -in  useradmin.maison.ca.csr
Certificate Request:
    Data:
        Version: 0 (0x0)
        Subject: C=CA, ST=Quebec, L=Montreal, O=Anemone, OU=Tech.Support, CN=useradmin.maison.ca/emailAddress=jadupl2@gmail.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
            RSA Public Key: (1024 bit)
                Modulus (1024 bit):
                    00:c5:d5:be:34:b9:a7:e0:ce:73:48:1b:a8:72:bc:
                    cb:05:ba:88:c4:6d:b4:e8:f8:7d:a1:5a:d0:48:ae:
                    08:0b:6a:38:4b:b5:3b:df:41:6e:b5:45:d6:83:38:
                    98:b0:1c:57:25:86:18:25:c8:4b:ee:75:0d:82:8d:
                    08:03:2d:78:5a:52:d1:d8:f8:bb:fd:18:3c:27:3a:
                    dd:4f:67:73:8d:07:c2:4a:8f:5c:b0:51:70:f3:2e:
                    f0:88:ae:97:ef:ac:d8:e5:e6:2a:c3:54:3d:d3:5f:
                    45:ec:b6:8f:b5:a3:1b:fa:b8:3c:66:53:0b:98:b1:
                    28:ce:a9:20:8d:43:6d:24:bb
                Exponent: 65537 (0x10001)
        Attributes:
            challengePassword        :SH221bbs
    Signature Algorithm: sha1WithRSAEncryption
        7d:33:4d:16:3a:6d:7c:f7:ef:1f:d3:40:6f:82:d9:1c:40:0d:
        33:4c:a6:4d:66:20:71:7e:97:7f:0b:16:73:8e:5e:ca:9f:ec:
        10:df:45:c0:d8:2e:a1:b6:bc:c3:0c:ec:7b:90:45:19:36:78:
        e6:8d:eb:39:b6:18:9b:62:2f:dc:1c:12:80:f3:ff:93:46:c5:
        12:19:bd:ee:87:6c:11:2e:6a:e9:b1:0e:6d:3f:12:5a:05:88:
        ef:77:3d:2c:06:82:8d:ca:57:b2:8c:bd:22:d1:0d:8e:89:85:
        34:03:de:77:cf:22:38:b8:c1:7d:c0:71:e3:88:10:6a:32:6b:
        48:2e
-bash-3.1#


Creating a Self-Signed Certificate

-bash-3.1# openssl x509 -req -days 30 -in useradmin.maison.ca.csr -signkey useradmin.maison.ca -out useradmin.maison.ca.cert Signature ok
subject=/C=CA/ST=Quebec/L=Montreal/O=Anemone/OU=Tech.Support/CN=useradmin.maison.ca/emailAddress=jadupl2@gmail.com
Getting Private key
-bash-3.1#


-bash-3.1# ls -ltr useradmin.maison*
-rw-r--r-- 1 root security 891 Jan 21 11:09 useradmin.maison.ca
-rw-r--r-- 1 root security 753 Jan 21 11:17 useradmin.maison.ca.csr
-rw-r--r-- 1 root security 989 Jan 21 11:24 useradmin.maison.ca.cert
-bash-3.1# cat  useradmin.maison.ca.cert
-----BEGIN CERTIFICATE-----
MIICrTCCAhYCCQCI3Ii7rTcEEzANBgkqhkiG9w0BAQUFADCBmjELMAkGA1UEBhMC
Q0ExDzANBgNVBAgTBlF1ZWJlYzERMA8GA1UEBxMITW9udHJlYWwxEDAOBgNVBAoT
B0FuZW1vbmUxFTATBgNVBAsTDFRlY2guU3VwcG9ydDEcMBoGA1UEAxMTdXNlcmFk
bWluLm1haXNvbi5jYTEgMB4GCSqGSIb3DQEJARYRamFkdXBsMkBnbWFpbC5jb20w
HhcNMDcwMTIxMTYyNDAyWhcNMDcwMjIwMTYyNDAyWjCBmjELMAkGA1UEBhMCQ0Ex
DzANBgNVBAgTBlF1ZWJlYzERMA8GA1UEBxMITW9udHJlYWwxEDAOBgNVBAoTB0Fu
ZW1vbmUxFTATBgNVBAsTDFRlY2guU3VwcG9ydDEcMBoGA1UEAxMTdXNlcmFkbWlu
Lm1haXNvbi5jYTEgMB4GCSqGSIb3DQEJARYRamFkdXBsMkBnbWFpbC5jb20wgZ8w
DQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMXVvjS5p+DOc0gbqHK8ywW6iMRttOj4
faFa0EiuCAtqOEu1O99BbrVF1oM4mLAcVyWGGCXIS+51DYKNCAMteFpS0dj4u/0Y
PCc63U9nc40HwkqPXLBRcPMu8Iiul++s2OXmKsNUPdNfRey2j7WjG/q4PGZTC5ix
KM6pII1DbSS7AgMBAAEwDQYJKoZIhvcNAQEFBQADgYEAAnKkTT3PZVvZuq1fM8hR
6jfBR8hTXGeB0IRdxVGqtN/DyqZxwgKjLIIAhxRhBSwW6qWObTjrZh46QDI0Q8mx
fA6hVVPil7pwaFaAmkVTlLcN9HwC+1WlrT8IuvkzpmUFQBPXwPHLmMH+LcggVZCo
glY/G0LG2QD6ARnu7ZHIfck=
-----END CERTIFICATE-----
-bash-3.1#
````

## Change owner of certificate files

```bash
-bash-3.1# chown secadm1.security *
-bash-3.1# ls -l
total 104
-rw-r--r-- 1 secadm1 security    36 Nov 20 08:45 home_linux_servers.txt
-rw-r--r-- 1 secadm1 security 34705 Jan 21 11:28 httpd_linux.cfg
-rw-r--r-- 1 secadm1 security 23396 Nov  1 15:48 httpd_useradmin.conf
-rw-r--r-- 1 secadm1 security   464 Jan  2 11:16 linux_servers.txt
-rw-r--r-- 1 secadm1 security   384 Nov 17 20:50 office_aix_server.txt
-rw-r--r-- 1 secadm1 security   384 Nov 17 20:50 office_linux_servers.txt
-rw-r--r-- 1 secadm1 security   384 Nov 20 08:47 office_linux_server.txt
-rwxr-xr-x 1 secadm1 security   410 Nov  1 15:55 useradmin_add.sh
-rw-r--r-- 1 secadm1 security    26 Nov  2 09:55 useradmin_groups
-rw-r--r-- 1 secadm1 security   891 Jan 21 11:09 useradmin.maison.ca
-rw-r--r-- 1 secadm1 security   989 Jan 21 11:24 useradmin.maison.ca.cert
-rw-r--r-- 1 secadm1 security   753 Jan 21 11:17 useradmin.maison.ca.csr
-rw-r--r-- 1 secadm1 security    44 Nov  2 09:54 useradmin_users
-bash-3.1#
````

## Stop Server 

```bash
-bash-3.1# ps -ef | grep useradmin
root      2143     1  0 Jan15 ?        00:00:10 httpd -f /useradmin/cfg/httpd_linux.cfg
secadm1   2154  2143  0 Jan15 ?        00:00:00 httpd -f /useradmin/cfg/httpd_linux.cfg
secadm1   2155  2143  0 Jan15 ?        00:00:00 httpd -f /useradmin/cfg/httpd_linux.cfg
secadm1   2156  2143  0 Jan15 ?        00:00:00 httpd -f /useradmin/cfg/httpd_linux.cfg
secadm1   2157  2143  0 Jan15 ?        00:00:00 httpd -f /useradmin/cfg/httpd_linux.cfg
secadm1   2161  2143  0 Jan15 ?        00:00:00 httpd -f /useradmin/cfg/httpd_linux.cfg
secadm1   2162  2143  0 Jan15 ?        00:00:00 httpd -f /useradmin/cfg/httpd_linux.cfg
secadm1   2163  2143  0 Jan15 ?        00:00:00 httpd -f /useradmin/cfg/httpd_linux.cfg
secadm1   2164  2143  0 Jan15 ?        00:00:00 httpd -f /useradmin/cfg/httpd_linux.cfg
root     26331 26252  0 10:42 pts/1    00:00:00 tail -f useradmin_error.log
root     26662 26358  0 11:30 pts/2    00:00:00 grep useradmin
-bash-3.1# kill `cat /useradmin/cfg/httpd_useradmin.pid`
-bash-3.1# ps -ef | grep useradmin
root     26331 26252  0 10:42 pts/1    00:00:00 tail -f useradmin_error.log
root     26665 26358  0 11:30 pts/2    00:00:00 grep useradmin
-bash-3.1#
```

## Modify the http config file

```bash
<VirtualHost 192.168.1.123>
ServerAdmin             jacques.duplessis@videoron.ca
SSLEngine               on
SSLCertificateFile      /useradmin/cfg/useradmin.maison.ca.cert
SSLCertificateKeyFile   /useradmin/cfg/useradmin.maison.ca
DocumentRoot            /useradmin
ServerName              useradmin.slac.ca
ServerAlias             useradmin
ErrorLog                /useradmin/logs/useradmin_error.log
TransferLog             /useradmin/logs/useradmin_access.log
</VirtualHost>
```


## Start HTTP Server

```bash
httpd -DSSL -f /useradmin/cfg/httpd_linux.cfg

-bash-3.1# httpd -f /useradmin/cfg/httpd_linux.cfg
Syntax error on line 1021 of /useradmin/cfg/httpd_linux.cfg:
Invalid command 'SSLCertificate', perhaps misspelled or defined by a module not included in the server configuration
-bash-3.1#
````
