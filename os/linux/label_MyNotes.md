[root@gumby ~]# fdisk -l 

#Disk /dev/hda: 250.0 GB, 250059350016 bytes
<u>255 heads, 63 sectors/track, 30401 cylinders
</u>**Units = cylinders of 16065 * 512 = 8225280 bytes

   Device Boot      Start         End      Blocks   Id  System
/dev/hda1               1        2550    20482843+   7  HPFS/NTFS
Partition 1 does not end on cylinder boundary.
/dev/hda2   *        2551        2583      265072+  83  Linux
/dev/hda3            2584        2844     2096482+  82  Linux swap / Solaris
/dev/hda4            2845       30401   221351602+   5  Extended
/dev/hda5            2845       27066   194563183+  8e  Linux LVM
/dev/hda6           27067       30401    26788356   83  Linux

Disk /dev/sda: 500.1 GB, 500107862016 bytes
255 heads, 63 sectors/track, 60801 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes

   Device Boot      Start         End      Blocks   Id  System
/dev/sda1               1       60801   488384001   83  Linux

Disk /dev/sdb: 80.0 GB, 80026361856 bytes
255 heads, 63 sectors/track, 9729 cylinders
Units = cylinders of 16065 * 512 = 8225280 bytes

   Device Boot      Start         End      Blocks   Id  System
/dev/sdb1   *           1           5       32768   83  Linux
Partition 1 does not end on cylinder boundary.
/dev/sdb2               5        9729    78115424   83  Linux
[root@gumby ~]# 


[root@gumby ~]# e2label /dev/sda1 500gb
[root@gumby ~]# 


[root@gumby ~]# grep bdisk  /etc/fstab
/dev/sda1                      /mnt/bdisk                     ext3 noauto 1   2  
[root@gumby ~]# vi /etc/fstab


[root@gumby ~]# grep bdisk  /etc/fstab
LABEL=500gb                    /mnt/bdisk                     ext3 noauto 1   2  
[root@gumby ~]# 

[root@gumby ~]# umount /mnt/bdisk
[root@gumby ~]# 
[root@gumby ~]# mount /mnt/bdisk
[root@gumby ~]# df -hP | grep bdisk
/dev/sda1             459G   89G  347G  21% /mnt/bdisk
[root@gumby ~]# 


[root@gumby ~]# 
[root@gumby ~]# 
[root@gumby ~]# df -h | grep sdb
/dev/sdb1              31M   11M   19M  37% /usb_boot
/dev/sdb2              75G  9.0G   62G  13% /usb_storix
[root@gumby ~]# grep sdb /etc/fstab
/dev/sdb1                      /usb_boot                      ext2 noauto 0   0  
/dev/sdb2                      /usb_storix                    ext2 noauto 0   0  
[root@gumby ~]# e2label /dev/sdb1 stboot
[root@gumby ~]# e2label /dev/sdb2 stdata
[root@gumby ~]# 

[root@gumby ~]# vi /etc/fstab

[root@gumby ~]# egrep "stboot|stdata"  /etc/fstab
LABEL=stboot                   /usb_boot                      ext2 noauto 0   0  
LABEL=stdata                   /usb_storix                    ext2 noauto 0   0  
[root@gumby ~]# 

[root@gumby ~]# umount /usb_boot
[root@gumby ~]# umount /usb_storix
[root@gumby ~]# mount /usb_boot
[root@gumby ~]# mount /usb_storix
[root@gumby ~]# 
[root@gumby ~]# df -h | grep sdb
/dev/sdb1              31M   11M   19M  37% /usb_boot
/dev/sdb2              75G  9.0G   62G  13% /usb_storix
[root@gumby ~]# 


[root@gumby ~]# findfs LABEL=stdata
/dev/sdb2
[root@gumby ~]# findfs LABEL=stboot
/dev/sdb1
[root@gumby ~]# findfs LABEL=500gb
/dev/sda1
[root@gumby ~]# 

[root@gumby ~]# mount -l 
/dev/sda1 on /mnt/bdisk type ext3 (rw) [500gb]
/dev/sdb1 on /usb_boot type ext2 (rw) [stboot]
/dev/sdb2 on /usb_storix type ext2 (rw) [stdata]
[root@gumby ~]# 

e2label


