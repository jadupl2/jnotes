# Notes Firewalld


## Stop/Start/Status of firewall service

```bash
[root@rhel8 ~]# systemctl stop firewalld
[root@rhel8 ~]# systemctl start firewalld
[root@rhel8 ~]# systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2019-08-21 10:27:15 EDT; 1s ago
     Docs: man:firewalld(1)
 Main PID: 5563 (firewalld)
    Tasks: 2 (limit: 11528)
   Memory: 20.4M
   CGroup: /system.slice/firewalld.service
           ├─5563 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid
           └─5910 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid

Aug 21 10:27:14 rhel8.maison.ca systemd[1]: Starting firewalld - dynamic firewall daemon...
Aug 21 10:27:15 rhel8.maison.ca systemd[1]: Started firewalld - dynamic firewall daemon.
[root@rhel8 ~]# 
```


## Listing firewall rules

```bash
root@holmes~ # firewall-cmd --list-all ; echo $?
FirewallD is not running
252
root@holmes~ #

[root@rhel8 ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens192
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 32/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[root@rhel8 ~]#
```

## Stop/Start/Status of fire

```bash
[root@rhel8 ~]# firewall-cmd --add-service=http
success
[root@rhel8 ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens192
  sources:
  services: cockpit dhcpv6-client http ssh
  ports: 32/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[root@rhel8 ~]#
```

## If we reload firewall rules, we will lost our changes (http services)

```bash
[root@rhel8 ~]# firewall-cmd --reload
success
[root@rhel8 ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens192
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 32/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```


## Listing permanent firewall rules
Our http service is not part of the permanent list.

```bash
[root@rhel8 ~]# firewall-cmd --list-all --permanent
public
  target: default
  icmp-block-inversion: no
  interfaces:
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 32/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[root@rhel8 ~]
```

### Enable a service (Permanent)

```bash
[root@rhel8 ~]# firewall-cmd --add-service=http --permanent
success
[root@rhel8 ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens192
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 32/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

Now our httpd service is part of our permanent rules

```bash
[root@rhel8 ~]# firewall-cmd --list-all --permanent
public
  target: default
  icmp-block-inversion: no
  interfaces:
  sources:
  services: cockpit dhcpv6-client http ssh
  ports: 32/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
``` 


If we reload the firewall rules, the httpd service is now part of it.

```bash
[root@rhel8 ~]# firewall-cmd --reload
success

[root@rhel8 ~]# firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: ens192
  sources:
  services: cockpit dhcpv6-client http ssh
  ports: 32/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[root@rhel8 ~]#
```

## Remove rule

```bash
root@rhel8 ~]# firewall-cmd --remove-service=http
success
[root@rhel8 ~]# firewall-cmd --remove-service=http --permanent
success
[root@rhel8 ~]#
```

## Copy Runtime rules to permanent with this command

```bash
[root@rhel8 ~]# firewall-cmd --runtime-to-permanent
success
[root@rhel8 ~]#
``` 
