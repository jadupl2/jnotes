# NFS on MacOS

## Show mount available on a server
`````
jacques@mycroftw:~/Documents/Dev/sadmin $ showmount -e holmes
Exports list on holmes:
/sadmin                             *
/install                            *
jacques@mycroftw:~/Documents/Dev/sadmin $
``````

## Mount remote mount point
````
jacques@mycroftw:~/Documents/Dev/sadmin $ sudo mkdir /private/nfs
Password:
jacques@mycroftw:~/Documents/Dev/sadmin $ sudo mount -t nfs holmes:/install /private/nfs
jacques@mycroftw:~/Documents/Dev/sadmin $ df -h /private/nfs
Filesystem        Size   Used  Avail Capacity iused   ifree %iused  Mounted on
holmes:/install   58Gi   34Gi   21Gi    63%    3371 3808661    0%   /private/nfs
jacques@mycroftw:~/Documents/Dev/sadmin $
jacques@mycroftw:~/Documents/Dev/sadmin $ sudo umount /private/nfs
````
To mount an NFS file system in read/write mode, enter:
````
$ sudo mount -o rw -t nfs nas01:/mp3 /private/nfs
````

## Tip: Operation not permitted Error

If you get an error which read as follows:

```` 192.168.3.1:/mp3 Operation not permitted ````

Try to mount it as follows with -o resvport command:
````
$ sudo mount -t nfs -o resvport 192.168.3.1:/mp3 /private/nfs 
````

OR mount an NFS in read/write mode, enter:
````
$ sudo mount -t nfs -o resvport,rw 192.168.3.1:/mp3 /private/nfs
````

## Recommend mount Command Options

I suggest that you run the mount command it as follows to get better a performance:
````
$ sudo mount -t nfs -o soft,intr,rsize=8192,wsize=8192,timeo=900,retrans=3,proto=tcp nas01:/sales /private/sales
````

OR
````
$ sudo mount -t nfs nfs -o soft,timeo=900,retrans=3,vers=3, proto=tcp nas01:/sales /private/sales
````

https://www.cyberciti.biz/faq/apple-mac-osx-nfs-mount-command-tutorial/
