# ssh-agent notes

### ssh-agent startup 
When started from your ~/.bash_profile, it will first check to see whether an ssh-agent is already running. If not, then it will start ssh-agent and record the important SSH_AUTH_SOCK and SSH_AGENT_PID variables in the ~/.ssh-agent file for safekeeping and later use. Here's the best way to start keychain; like using plain old ssh-agent, we perform the necessary setup inside ~/.bash_profile:
````
jacques@mycroftw:~ $ ssh-agent
SSH_AUTH_SOCK=/var/folders/y8/h5jmxcgj7dq087vr4ktrjjsh0000gn/T//ssh-d4f2NTmi3Hs3/agent.69891; export SSH_AUTH_SOCK;
SSH_AGENT_PID=69892; export SSH_AGENT_PID;
echo Agent pid 69892;
jacques@mycroftw:~ $
````

### ssh-add
Before we can really use ssh-agent, we first need to add add our private key(s) to ssh-agent's cache using the ssh-add command. ssh-add asked for my passphrase so that the private key can be decrypted and stored in ssh-agent's cache, ready for use.
````
jacques@mycroftw:~ $ ssh-add
Enter passphrase for /Users/jacques/.ssh/id_rsa:
Identity added: /Users/jacques/.ssh/id_rsa (/Users/jacques/.ssh/id_rsa)
Identity added: /Users/jacques/.ssh/id_ecdsa (/Users/jacques/.ssh/id_ecdsa)
jacques@mycroftw:~ $
````


### SSH to another server without prompt
As you can see, ssh-add asked for my passphrase so that the private key can be decrypted and stored in ssh-agent's cache, ready for use. Once you've used ssh-add to add your private key (orkeys) to ssh-agent's cache and SSH_AUTH_SOCK is defined in your current shell (which it should be, if you started ssh-agent from your ~/.bash_profile), then you can use scp and ssh to establish connections with remote systems without supplying your passphrase.
````
jacques@mycroftw:~ $ ssh -p32 jacques@holmes
Last login: Mon Oct 22 14:30:47 2018 from mycroftw.maison.ca
 _           _
| |__   ___ | |_ __ ___   ___  ___
| '_ \ / _ \| | '_ ` _ \ / _ \/ __|
| | | | (_) | | | | | | |  __/\__ \
|_| |_|\___/|_|_| |_| |_|\___||___/

jacques@holmes:~$
````