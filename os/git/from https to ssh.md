# Change access git remote repository from http to ssh

## List remote repositories before the change

```bash
$ cd /sadmin  
$ git remote -v  
github    https://github.com/jadupl2/sadmin.git (fetch)  
github    https://github.com/jadupl2/sadmin.git (push)  
gogs    http://gitserver.maison.ca:3000/jacques/sadmin.git (fetch)  
gogs    http://gitserver.maison.ca:3000/jacques/sadmin.git (push)  
origin    ssh://git@bitbucket.org:22/jadupl2/sadmin.git (fetch)  
origin    ssh://git@bitbucket.org:22/jadupl2/sadmin.git (push)  
$   
```

## Change GitHub remote repository URL

```bash
jacques@holmes:/sadmin$ git remote set-url github ssh://git@github.com:22/jadupl2/sadmin.git
```

## List remote repositories after the change

```bash
jacques@holmes:/sadmin$ git remote -v
github    ssh://git@github.com:22/jadupl2/sadmin.git (fetch)
github    ssh://git@github.com:22/jadupl2/sadmin.git (push)
gogs    http://gitserver.maison.ca:3000/jacques/sadmin.git (fetch)
gogs    http://gitserver.maison.ca:3000/jacques/sadmin.git (push)
origin    ssh://git@bitbucket.org:22/jadupl2/sadmin.git (fetch)
origin    ssh://git@bitbucket.org:22/jadupl2/sadmin.git (push)
```

## Test Change

```bash
jacques@holmes:~/.ssh$ cd /sadmin
jacques@holmes:/sadmin$ git push github master
Counting objects: 5, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 297 bytes | 0 bytes/s, done.
Total 3 (delta 2), reused 0 (delta 0)
remote: Resolving deltas: 100% (2/2), completed with 2 local objects.
To ssh://git@github.com:22/jadupl2/sadmin.git
   b4e2290..2bc4298  master -> master
jacques@holmes:/sadmin$ 
```
