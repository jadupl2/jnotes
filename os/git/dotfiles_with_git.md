# Best way to store your dotfiles

I've been looking for a better way to manage my dotfiles. My old method involved moving all of my dotfiles into their own directory and then symlinking each one fo them back into the directory structure. What a headache! Using git bare repositories, there is no more moving files into an initialized git repository and then creating symlinks. Now, I just add, commit and then push. Done. Want to make your own git bare repository? First, make a directory for your new git bare repository (I created one called "dotfiles" but you can name it whatever). Then I entered the following in the terminal:  

```bash
jacques@holmes:~$ git init --bare $HOME/dotfiles
Initialized empty Git repository in /home/jacques/dotfiles/
jacques@holmes:~$ ls -l $HOME/dotfiles
total 16
drwxrwxr-x 2 jacques sadmin    6 Feb  4 13:37 branches
-rw-rw-r-- 1 jacques sadmin   66 Feb  4 13:37 config
-rw-rw-r-- 1 jacques sadmin   73 Feb  4 13:37 description
-rw-rw-r-- 1 jacques sadmin   23 Feb  4 13:37 HEAD
drwxrwxr-x 2 jacques sadmin 4096 Feb  4 13:37 hooks
drwxrwxr-x 2 jacques sadmin   20 Feb  4 13:37 info
drwxrwxr-x 4 jacques sadmin   28 Feb  4 13:37 objects
drwxrwxr-x 4 jacques sadmin   29 Feb  4 13:37 refs
jacques@holmes:~$ 


$ alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'  
$  
$ echo "alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'" >> $HOME/.bashrc  
$  
jacques@holmes:~$ config config --local status.showUntrackedFiles no

jjacques@holmes:~$ config  add .bashrc .bash_profile
jacques@holmes:~$ config status
# On branch master
#
# Initial commit
#
# Changes to be committed:
#   (use "git rm --cached <file>..." to unstage)
#
#    new file:   .bash_profile
#    new file:   .bashrc
#
# Untracked files not listed (use -u option to show untracked files)
jacques@holmes:~$ config commit -m "Initial commit" 
[master (root-commit) 62fce7b] Initial commit
 2 files changed, 83 insertions(+)
 create mode 100755 .bash_profile
 create mode 100755 .bashrc
jacques@holmes:~$ 
```

### Basic usage example:

```bash
config add /path/to/file  
config commit -m "A short message"  
config push  
```
