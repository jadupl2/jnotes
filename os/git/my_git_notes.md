# MyNotes - Git 

- [MyNotes - Git](#mynotes---git)
  - [Git configuration](#git-configuration)
    - [User configuration setting example](#user-configuration-setting-example)
    - [Level of configuration](#level-of-configuration)
    - [Listing configuration](#listing-configuration)
  - [Change git access from https to ssh](#change-git-access-from-https-to-ssh)

## Git configuration
### User configuration setting example
```
$ git config --global user.name "Jacques Duplessis - MacBook"  
$ git config --global user.email duplessis.jacques@gmail.com  
$ git config --global core.editor vim  
$ git config --global merge.tool vimdiff  
$ git config --global color.ui color        (Important message or heading in color)  
$ git config --global commit.template $HOME/.gitmessage.txt  
```

### Level of configuration
```
$ git config --system. /etc/gitconfig             # System Global setting  
$ git config --global ~/.gitconfig                # User Setting  
$ git config .git/config                          # Repository Setting  
```

### Listing configuration
```
$ git config --list   
$ git config --global --list
user.name=Jacques Duplessis
user.email=duplessis.jacques@gmail.com
color.ui=true
core.editor=code --wait
diff.tool=default-difftool
difftool.default-difftool.cmd=code --wait --diff $LOCAL $REMOTE
merge.tool=code
```

## Change git access from https to ssh
```
$ git remote -v
origin	https://jadupl2@bitbucket.org/jadupl2/jnotes.git (fetch)
origin	https://jadupl2@bitbucket.org/jadupl2/jnotes.git (push)

$ git remote rm origin

$ git remote -v

$ git remote add origin ssh://git@bitbucket.org:22/jadupl2/jnotes.git

$ git remote -v
origin	ssh://git@bitbucket.org:22/jadupl2/jnotes.git (fetch)
origin	ssh://git@bitbucket.org:22/jadupl2/jnotes.git (push)
```


