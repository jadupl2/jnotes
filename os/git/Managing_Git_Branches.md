# Managing Branches <!-- omit in toc -->

## Table of content <!-- omit in toc -->
- [List local branch](#list-local-branch)
- [List remote branch](#list-remote-branch)
- [List local and remote branches](#list-local-and-remote-branches)
- [Creating local branch 'newbackup'](#creating-local-branch-newbackup)
- [Switch to new branch](#switch-to-new-branch)
- [Create Remote Branch](#create-remote-branch)
- [Switching back to our ‘master’ branch](#switching-back-to-our-master-branch)
- [Create a new branch and switch to it](#create-a-new-branch-and-switch-to-it)
- [Delete a local branch](#delete-a-local-branch)
- [Delete remote branches](#delete-remote-branches)
- [Rename a local branch](#rename-a-local-branch)
- [Rename a remote branch](#rename-a-remote-branch)
- [Example : Flow of using branch](#example--flow-of-using-branch)

## List local branch
```bash
$ git branch
* master
```

## List remote branch
```bash
$ git branch -r
  gogs/master
  origin/master
```

## List local and remote branches
```bash
$ git branch -a
* master
  remotes/gogs/master
  remotes/origin/master
``` 


## Creating local branch 'newbackup'
```bash
$ git branch newbackup
$ git branch -a
* master
  newbackup
  remotes/gogs/master
  remotes/origin/master
$ git status
On branch master
Your branch is up-to-date with 'gogs/master'.
nothing to commit, working tree clean
```

## Switch to new branch
```bash
$ git checkout newbackup
```

We are now on our new branch
```bash
$ git branch -a
  master
* newbackup
  remotes/gogs/master
  remotes/origin/master
$ git status
On branch newbackup
nothing to commit, working tree clean
```

## Create Remote Branch
Make some changes to your code , save it and commit it locally.  
Make sure the ‘git status’ is clean before doing a ‘git checkout | push | pull command’.  
Push your changes to your remote sites.  
```bash
$ git push origin newbackup
Password for 'https://jadupl2@bitbucket.org': 
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 415 bytes | 0 bytes/s, done.
Total 4 (delta 3), reused 0 (delta 0)
remote: 
remote: Create pull request for newbackup:
remote:   https://bitbucket.org/jadupl2/sadmin/pull-requests/new?source=newbackup&t=1
To https://bitbucket.org/jadupl2/sadmin.git
 * [new branch]      newbackup -> newbackup

$ git push gogs newbackup
Username for 'http://gitserver.maison.ca:3000': jacques
Password for 'http://jacques@gitserver.maison.ca:3000': 
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 415 bytes | 0 bytes/s, done.
Total 4 (delta 3), reused 0 (delta 0)
To http://gitserver.maison.ca:3000/jacques/sadmin.git
 * [new branch]      newbackup -> newbackup
```

We now have remote copy of our branch

```bash
jacques@holmes:/sadmin$ git branch -a
  master
* newbackup
  remotes/gogs/master
  remotes/gogs/newbackup
  remotes/origin/master
  remotes/origin/newbackup
jacques@holmes:/sadmin$ 
```


## Switching back to our ‘master’ branch

```bash
jacques@holmes:/sadmin$ git status
On branch newbackup
nothing to commit, working tree clean

jacques@holmes:/sadmin$ git checkout master
Switched to branch 'master'
Your branch is up-to-date with 'gogs/master'.

jacques@holmes:/sadmin$ git status
On branch master
Your branch is up-to-date with 'gogs/master'.
nothing to commit, working tree clean
```


## Create a new branch and switch to it
```bash
jacques@yoda:~/Documents/sadmin$ git checkout -b newui
jacques@yoda:~/Documents/sadmin$ git status

On branch newui
nothing to commit, working directory clean
jacques@yoda:~/Documents/sadmin$ git branch
  master
* newui
jacques@yoda:~/Documents/sadmin$ 
```

## Delete a local branch
If you create a branch by mistake, can delete it using the ‘-d’ option.
```bash
$ git branch -d branch_name
$ git branch -D branch_name (Delete without worrying about merge status)
```

## Delete remote branches
```bash
$ git status
# On branch master
nothing to commit, working directory clean

$ git branch -a
* master
  remotes/github/master
  remotes/gogs/master
  remotes/gogs/textbelt
  remotes/origin/master
  remotes/origin/textbelt

$ git push origin --delete textbelt
To ssh://git@bitbucket.org:22/jadupl2/sadmin.git
 - [deleted]         textbelt

$ git push gogs --delete textbelt
Username for 'http://gitserver.maison.ca:3000': jacques
Password for 'http://jacques@gitserver.maison.ca:3000': 
To http://gitserver.maison.ca:3000/jacques/sadmin.git
 - [deleted]         textbelt

$ git branch -a
* master
  remotes/github/master
  remotes/gogs/master
  remotes/origin/master
```

## Rename a local branch
If you are on the branch you want to rename:
```bash
$ git branch -m new-name
```

If you are on a different branch :
```bash
jacques@holmes:/sadmin$ git branch -a
* master
  newbackup
  remotes/gogs/master
  remotes/gogs/newbackup
  remotes/origin/master
  remotes/origin/newbackup
jacques@holmes:/sadmin$ 

jacques@holmes:/sadmin$ git branch -m  newbackup wip
jacques@holmes:/sadmin$ 

jacques@holmes:/sadmin$ git status
On branch master
Your branch is up-to-date with 'gogs/master'.
nothing to commit, working tree clean

jacques@holmes:/sadmin$ git branch -a
* master
  wip
  remotes/gogs/master
  remotes/gogs/newbackup
  remotes/origin/master
  remotes/origin/newbackup
jacques@holmes:/sadmin$ 
```

## Rename a remote branch 
List the Branches before the rename 
```bash
jacques@holmes:/sadmin$ git branch -a
* master
  wip
  remotes/gogs/master
  remotes/gogs/newbackup
  remotes/origin/master
  remotes/origin/newbackup
```

Delete the old-name remote branch and push the new-name local branch
```bash
jacques@holmes:/sadmin$ git push origin :newbackup wip
Password for 'https://jadupl2@bitbucket.org': 
Total 0 (delta 0), reused 0 (delta 0)
To https://bitbucket.org/jadupl2/sadmin.git
 - [deleted]         newbackup
 * [new branch]      wip -> wip
```

Reset the upstream branch for the new-name local branch, Switch to the branch and then:
```bash
jacques@holmes:/sadmin$ git checkout wip
Switched to branch 'wip'
Your branch is up-to-date with 'gogs/wip'.

jacques@holmes:/sadmin$ git push origin -u wip
Password for 'https://jadupl2@bitbucket.org': 
Branch wip set up to track remote branch wip from origin.
Everything up-to-date
jacques@holmes:/sadmin$ 
```

List the Branches after the rename 
```bash
jacques@holmes:/sadmin$ git branch -a
  master
* wip
  remotes/gogs/master
  remotes/gogs/newbackup
  remotes/origin/master
  remotes/origin/wip
```
Do the same thing for the other remote site.  


## Example : Flow of using branch

This tutorial explains the following steps:
1. Create a new dev branch  
2. Do your work on local dev branch  
3. Push dev branch from your local to central git repository  
4. Once your work is done, merge dev branch to master  
5. Finally, delete the dev branch from both local and central git repository  

```bash
# Clone the git repo:
git clone https://remote-git-repo-url/demoproject  

# Create new dev branch, do your work, and commit changes locally
git checkout -b dev
vi index.html
git commit -m "Made the change.." index.html  

# Push your changes to remote dev branch
git push --set-upstream origin dev  

# Merge dev branch to master
git checkout master
git merge dev  

# Finally, delete dev branch both locally and remote
git branch -d dev
git branch -d -r origin/dev  
```

