# Gitlab Notes

## Test ssh key

```bash

jacques@holmes:~/Documents/Dev/jnotes$ ssh -T git@gitlab.maison.ca
Warning: Permanently added the ECDSA host key for IP address '[192.168.1.142]:32' to the list of known hosts.
Welcome to GitLab, @jacques!
```
## Add gitlab.maison.ca Repo

```bash
~/Documents/Dev/jnotes$ git remote -v
gitlab	ssh://git@gitlab.maison.ca:22/jacques/jnotes.git (fetch)
gitlab	ssh://git@gitlab.maison.ca:22/jacques/jnotes.git (push)
origin	ssh://git@bitbucket.org:22/jadupl2/jnotes.git (fetch)
origin	ssh://git@bitbucket.org:22/jadupl2/jnotes.git (push)

~/Documents/Dev/jnotes$ git remote set-url gitlab ssh://git@gitlab.maison.ca:32/jacques/jnotes.git

~/Documents/Dev/jnotes$ git remote -v
gitlab	ssh://git@gitlab.maison.ca:32/jacques/jnotes.git (fetch)
gitlab	ssh://git@gitlab.maison.ca:32/jacques/jnotes.git (push)
origin	ssh://git@bitbucket.org:22/jadupl2/jnotes.git (fetch)
origin	ssh://git@bitbucket.org:22/jadupl2/jnotes.git (push)
~/Documents/Dev/jnotes$
```




  * template[/var/opt/gitlab/postgres-exporter/queries.yaml] action create (up to date)
Recipe: gitlab::deprecate-skip-auto-migrations
  * file[/etc/gitlab/skip-auto-reconfigure] action create (skipped due to only_if)
  * ruby_block[skip-auto-migrations deprecation] action run (skipped due to only_if)
Recipe: gitlab::unicorn
  * service[unicorn] action restart
    - restart service service[unicorn]
Recipe: gitaly::enable
  * service[gitaly] action restart
    - restart service service[gitaly]

Running handlers:
Running handlers complete
Chef Client finished, 17/599 resources updated in 01 minutes 03 seconds
gitlab Reconfigured!
Checking for an omnibus managed postgresql: OK
Checking for a newer version of PostgreSQL to install
No new version of PostgreSQL installed, nothing to upgrade to
Ensuring PostgreSQL is updated: OK
Restarting previously running GitLab services
ok: run: alertmanager: (pid 12412) 0s
ok: run: gitaly: (pid 12365) 2s
ok: run: gitlab-monitor: (pid 12430) 1s
ok: run: gitlab-workhorse: (pid 12434) 0s
ok: run: logrotate: (pid 12441) 1s
ok: run: nginx: (pid 12448) 0s
ok: run: node-exporter: (pid 12458) 1s
ok: run: postgres-exporter: (pid 12463) 0s
ok: run: postgresql: (pid 7588) 156096s
ok: run: prometheus: (pid 12472) 0s
ok: run: redis: (pid 7577) 156096s
ok: run: redis-exporter: (pid 12481) 1s
ok: run: sidekiq: (pid 12345) 9s
ok: run: unicorn: (pid 12490) 0s

     _______ __  __          __
    / ____(_) /_/ /   ____ _/ /_
   / / __/ / __/ /   / __ `/ __ \
  / /_/ / / /_/ /___/ /_/ / /_/ /
  \____/_/\__/_____/\__,_/_.___/


Upgrade complete! If your GitLab server is misbehaving try running
  sudo gitlab-ctl restart
before anything else.
If you need to roll back to the previous version you can use the database
backup made during the upgrade (scroll up for the filename).


