# CentOS 6, 7, RHEL, Fedora

## Download Gitkraken (rpm,deb,...)

- Go to the [download page](https://www.gitkraken.com/download)

## Install Gitkraken

```bash
jacques@holmes:~/Downloads$ sudo yum install gitkraken-amd64.rpm
Loaded plugins: fastestmirror, langpacks
Examining gitkraken-amd64.rpm: gitkraken-5.0.2-1.x86_64
Marking gitkraken-amd64.rpm as an update to gitkraken-4.2.2-1.x86_64
Resolving Dependencies
--> Running transaction check
---> Package gitkraken.x86_64 0:4.2.2-1 will be updated
---> Package gitkraken.x86_64 0:5.0.2-1 will be an update
--> Finished Dependency Resolution

Dependencies Resolved

===================================================================================================================
 Package                    Arch                    Version                   Repository                         Size
===================================================================================================================
Updating:
 gitkraken                  x86_64                  5.0.2-1                   /gitkraken-amd64              290 M

Transaction Summary
===================================================================================================================
Upgrade  1 Package

Total size: 290 M
Is this ok [y/d/N]: y
Downloading packages:
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
Warning: RPMDB altered outside of yum.
  Updating   : gitkraken-5.0.2-1.x86_64                                                                           1/2 
  Cleanup    : gitkraken-4.2.2-1.x86_64                                                                           2/2 
  Verifying  : gitkraken-5.0.2-1.x86_64                                                                           1/2 
  Verifying  : gitkraken-4.2.2-1.x86_64                                                                           2/2 

Updated:
  gitkraken.x86_64 0:5.0.2-1                                                                                          

Complete!
jacques@holmes:~/Downloads$ 
```

GitKraken can be run on certain configurations of CentOS, Fedora, and Red Hat Enterprise Linux (RHEL) , but we cannot guarantee it will work with your configuration. Kudos to Linux Hint for outlining some workarounds to help install on these systems!

```
yum install dnf
dnf install libXScrnSaver
dnf check-update && dnf upgrade

wget https://release.gitkraken.com/linux/gitkraken-amd64.tar.gz
tar -xvzf gitkraken-amd64.tar.gz
sudo rsync -va --delete-after gitkraken/ /opt/GitKraken/
cd /opt/GitKraken
./gitkraken
```

# Known issues on Linux

## Error with libcurl.so.4

Error: libcurl.so.4: cannot open shared object file: No such file or directory.

Run the following command to address the dependency issue.

```
sudo apt install libcurl3
```

## Error with libcurl-gnutls.so.4

libcurl-gnutls.so.4: cannot open shared object file: No such file or directory

Run the following command to address this issue.

```
sudo ln -s /usr/lib64/libcurl.so.4 /usr/lib64/libcurl-gnutls.so.4

Error with libXss.so.1
```

./gitkraken ./gitkraken: error while loading shared libraries: libXss.so.1: cannot open shared object file: No such file or directory

Run the following command to address this issue.

```
/usr/bin/dnf whatprovides libXss.so.1
dnf install libXScrnSaver
dnf check-update
dnf upgrade
```
