# Git Basic Configuration  <!-- omit in toc -->

### Table of content <!-- omit in toc -->
- [Command line user configuration example](#command-line-user-configuration-example)
  - [Level of configuration](#level-of-configuration)
  - [Listing configuration](#listing-configuration)
- [My git configuration](#my-git-configuration)
- [Making VSCode your default editor for git](#making-vscode-your-default-editor-for-git)
- [To edit your global .gitconfig file with vscode enter the following command ;](#to-edit-your-global-gitconfig-file-with-vscode-enter-the-following-command)
- [Alias you could create](#alias-you-could-create)
- [Making VSCode your default diff and merge tool](#making-vscode-your-default-diff-and-merge-tool)
- [Using vscode as a merge tool](#using-vscode-as-a-merge-tool)
- [Using vscode as a diff tool](#using-vscode-as-a-diff-tool)
- [Setup to use DiffMerge Tools](#setup-to-use-diffmerge-tools)
- [Settings for Linux](#settings-for-linux)
- [Setting for Windows (MSysGit) or Git Cmd](#setting-for-windows-msysgit-or-git-cmd)
  
---
### Command line user configuration example
```bash
$ git config --global user.name "Jacques Duplessis - MacBook"
$ git config --global user.email duplessis.jacques@gmail.com
$ git config --global core.editor vim
$ git config --global merge.tool vimdiff
$ git config --global color.ui color        (Important message or heading in color)
$ git config --global commit.template $HOME/.gitmessage.txt
```

#### Level of configuration
```bash
$ git config --system. /etc/gitconfig             # System Global setting
$ git config --global ~/.gitconfig                # User Setting
$ git config .git/config                        # Repository Setting
```

#### Listing configuration
```bash
$ git config --list

$ git config --global --list
user.name=Jacques Duplessis
user.email=duplessis.jacques@gmail.com
color.ui=true
core.editor=code --wait
diff.tool=default-difftool
difftool.default-difftool.cmd=code --wait --diff $LOCAL $REMOTE
merge.tool=code
```
#

### My git configuration
```bash
jacques@holmes:~$ cat .gitconfig 
[push]
    default = simple
[gui]
    recentrepo = /mystuff
    recentrepo = /sadmin
[user]
    name = Jacques Duplessis - Dell Linux
    email = duplessis.jacques@gmail.com
[core]
    editor = code --wait
[diff]
    tool = diffmerge
[difftool "default-difftool"]
    cmd = code --wait --diff $LOCAL $REMOTE
[merge]
    tool = diffmerge
[color]
    status = auto
    ui = true
[color "status"]
  added = green
  changed = blue
  untracked = magenta
  deleted = red
[alias]
lg1 = log --graph --all --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(bold white)— %an%C(reset)%C(bold yellow)%d%C(reset)' --abbrev-commit --date=relative
lg2 = log --graph --all --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(bold white)— %an%C(reset)' --abbrev-commit
lg  = !"git lg1"
gst = "git status"

[mergetool "vscode"]
    cmd = "code --wait "
[difftool "vscode"]
    cmd = "code --wait --diff  "
[difftool "diffmerge"]
    cmd = diffmerge \"$LOCAL\" \"$REMOTE\"
[mergetool "diffmerge"]
    cmd = /usr/bin/diffmerge --merge --result=\"$MERGED\" \"$LOCAL\" \"$BASE\" \"$REMOTE\"
    trustExitCode = true
[mergetool]
    keepBackup = false
```

### Making VSCode your default editor for git
```bash
$ git config --global core.editor "code --wait"
$ cat .gitconfig
[user]
    name = Jacques Duplessis
    email = duplessis.jacques@gmail.com
[color]
    ui = color
[core]
    editor = code --wait
root@holmes:~#
```

### To edit your global .gitconfig file with vscode enter the following command ;
```bash
$ git config --global -e
```

### Alias you could create
You can define aliases using the git config command—for example, running git config --global --add alias.st status will make running git st do the same thing as running git status—but I find when defining aliases, it's frequently easier to just edit the ~/.gitconfig file directly.
If you choose to go this route, you'll find that the ~/.gitconfig file is an INI file. INI is basically a key-value file format with particular sections. When adding an alias, you'll be changing the [alias] section. For example, to define the same git st alias as above, add this to the file:
```bash
[alias]
st = status

```
(If there's already an [alias] section, just add the second line to that existing section.)

### Making VSCode your default diff and merge tool
```bash
$ git config --global diff.tool vscode
$ git config --global difftool.vscode.cmd "code --wait --diff $LOCAL $REMOTE"
$ 
$ git config --global merge.tool vscode
$ git config --global mergetool.vscode.cmd "code --wait $MERGED"
$ 
```

### Using vscode as a merge tool
```bash
$ git mergetool
No files need merging
```

### Using vscode as a diff tool
This leverages the --diff option you can pass to VS Code to compare 2 files side by side.
Example : 
```bash
~$ code --diff file1.txt file2.txt

jacques@holmes:/sadmin/lib$ echo  "coco1" > coco1.txt
jacques@holmes:/sadmin/lib$ echo  "coco1" > coco2.txt
jacques@holmes:/sadmin/lib$ echo  "coco2" > coco2.txt
jacques@holmes:/sadmin/lib$ echo  "coco3" > coco1.txt
jacques@holmes:/sadmin/lib$ 
jacques@holmes:/sadmin/lib$ code --diff coco1.txt coco2.txt
```

### Setup to use DiffMerge Tools
Settings for OS X
First confirm that /usr/local/bin/diffmerge is present. If you used the PKG Installer, this was installed when /Applications/DiffMerge.app was installed. If you used the DMG file, refer to the instructions for installing the Extras. http://twobitlabs.com/2011/08/install-diffmerge-git-mac-os-x/

The following commands will update your .gitconfig to let GIT use DiffMerge:
```bash
git config --global diff.tool diffmerge
git config --global difftool.diffmerge.cmd 'diffmerge "$LOCAL" "$REMOTE"'
git config --global merge.tool diffmerge
git config --global mergetool.diffmerge.cmd 'diffmerge --merge --result="$MERGED" "$LOCAL" "$(if test -f "$BASE"; then echo "$BASE"; else echo "$LOCAL"; fi)" "$REMOTE"'
git config --global mergetool.diffmerge.trustExitCode true
git config --global mergetool.keepBackup false
```

### Settings for Linux
The following commands will update your .gitconfig to let GIT use DiffMerge:
```bash
$ git config --global diff.tool diffmerge
$ git config --global difftool.diffmerge.cmd "/usr/bin/diffmerge \"\$LOCAL\" \"\$REMOTE\""
$ git config --global merge.tool diffmerge
$ git config --global mergetool.diffmerge.trustExitCode true
$ git config --global mergetool.diffmerge.cmd "/usr/bin/diffmerge --merge --result=\"\$MERGED\" \"\$LOCAL\" \"\$BASE\" \"\$REMOTE\""
```

### Setting for Windows (MSysGit) or Git Cmd
```bash
C:\> git config --global diff.tool diffmerge
C:\> git config --global difftool.diffmerge.cmd
    "C:/Program\ Files/SourceGear/Common/DiffMerge/sgdm.exe
        \"$LOCAL\" \"$REMOTE\""

C:\> git config --global merge.tool diffmerge
C:\> git config --global mergetool.diffmerge.trustExitCode true
C:\> git config --global mergetool.diffmerge.cmd 
    "C:/Program\ Files/SourceGear/Common/DiffMerge/sgdm.exe
        -merge -result=\"$MERGED\" \"$LOCAL\" \"$BASE\" \"$REMOTE\""
```

