# Delete all messages from a channel
slack-cleaner --token <TOKEN> --message --channel general --user "*"

Legacy Token
xoxp-302328332919-300720211777-448797547569-708c9b49a71461d6efc27a28c37d270e

root@holmes:/sadmin/usr/bin$ slack-cleaner --token=xoxp-302328332919-300720211777-448797547569-708c9b49a71461d6efc27a28c37d270e --message --channel sadm_dev --user "*"
Running slack-cleaner v0.3.0
Channel, direct message or private group not found
root@holmes:/sadmin/usr/bin$ 

root@holmes:/sadmin/usr/bin$ slack-cleaner --token=xoxp-302328332919-300720211777-448797547569-708c9b49a71461d6efc27a28c37d270e --message --group sadm_dev --user "*"
Running slack-cleaner v0.3.0

0 message(s) will be cleaned.

Now you can re-run this program with `--perform` to actually perform the task.

root@holmes:/sadmin/usr/bin$ slack-cleaner --token=xoxp-302328332919-300720211777-448797547569-708c9b49a71461d6efc27a28c37d270e --message --group sadm_dev --user "*" --perform
Running slack-cleaner v0.3.0

0 message(s) cleaned.

root@holmes:/sadmin/usr/bin$ slack-cleaner --token=xoxp-302328332919-300720211777-448797547569-708c9b49a71461d6efc27a28c37d270e --message --channel sadm_dev --user "*" --perform
Running slack-cleaner v0.3.0
Channel, direct message or private group not found
root@holmes:/sadmin/usr/bin$ 

For Public Channel
root@holmes:/sadmin/usr/bin$ slack-cleaner --token=xoxp-302328332919-300720211777-448797547569-708c9b49a71461d6efc27a28c37d270e --message --channel sadm_prod --user "*"
Running slack-cleaner v0.3.0
No more messsages

0 message(s) will be cleaned.

Now you can re-run this program with `--perform` to actually perform the task.

root@holmes:/sadmin/usr/bin$ 


slack-cleaner --token=xoxp-302328332919-300720211777-448797547569-708c9b49a71461d6efc27a28c37d270e --message --group sadm_dev --bot --perform




