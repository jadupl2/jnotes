# Reference Markdown

# This is an # tag

## This is an ## tag

### This is [[My_Notes_Markdown.md]] My_Notes_Markdown.mdan ### tag

#### This is an #### tag

##### This is an ##### tag

###### This is an ###### tag

## Horizontal Lines

******

------

## Bold & Italic

*This text will be italic*  
_This will also be italic_  
**This text will be bold**  
__This will also be bold__  
*You **can** combine them*  
superscript^2^  
~~strikethrough~~  

## Unordered list

* Item 1
* Item 2
  * Item 2a
  * Item 2b

## Ordered list

1. Item 1
2. Item 2
3. Item 3
   * Item 3a
   * Item 3b

## Image

Images on the web or local files in the same directory:
![alt text](http://example.com/logo.png)..
![Logo](./logo.png)..

## Links

[GitHub](http://github.com)

## Block Notes

As Grace Hopper said:

> I’ve always been more interested
> in the future than in the past.
> 
> Example 1
> Example 1
> 
> > Example 2
> > Example 2
> > 
> > > Example 3
> > > Example 3..

## Code

Inline Code
We defined the `add` function to
compute the sum of two numbers.

Make a code chunk with three back ticks followed
by an r in braces. End the chunk with three back
ticks:

```{r}
paste("Hello", "World!")
```

```javascript
function test() {
console.log("look ma’, no spaces");
}
```

## Task Lists

- [x] this is a complete item
- [ ] this is an incomplete item
- [x] @mentions, #refs, [links](),
  **formatting**, and <del>tags</del>
  supported
- [x] list syntax required (any
  unordered or

## Tables

| First Header     | Second Header    |
| ---------------- | ---------------- |
| Content cell 1   | Content cell 2   |
| Content column 1 | Content column 2 |

## Issue Reference

#1  
github-flavored-markdown#1  
defunkt/github-flavored-markdown#1  
