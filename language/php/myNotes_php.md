# PHP Notes

- [PHP Notes](#php-notes)
  - [Open, Read and Close file](#open-read-and-close-file)
  - [PHP Arithmetic Operators](#php-arithmetic-operators)
  - [PHP Arithmetic Operators](#php-arithmetic-operators-1)
  - [PHP Increment / Decrement Operators](#php-increment--decrement-operators)
  - [PHP Logical Operators](#php-logical-operators)
  - [The PHP array operators are used to compare arrays.](#the-php-array-operators-are-used-to-compare-arrays)
  - [Interactive from command line](#interactive-from-command-line)

## Open, Read and Close file

```php
    $count=0;                                                           # Set Line Counter to Zero
    try
    {
      if ( !file_exists($WNAME) ) { throw new Exception('File not found.'); }
      $fh = fopen($WNAME, "r");
      if ( !$fh ) { throw new Exception('File open failed.'); } 
    }
    catch ( Exception $e ) {
        sadm_fatal_error ("Unable to open file " . $WNAME);
        exit();
    } 

    echo "<code>";
    while(!feof($fh)) {                                                 # Read till End Of File
        $wline = fgets($fh);                                            # Read Line By Line    
        if (strlen($wline) > 0) {                                       # Don't process empty Line
            $count+=1;                                                  # Increase Line Counter
            $pline = sprintf("%06d - %s\n" , $count,trim($wline));      # Format Line
            echo "<br>" . $pline  ;                                     # Print Log Line
        }
    }
    echo "</br></code></div>";                                          # End Data Frame
    fclose($fh);                                                        # Close Log
```


## PHP Arithmetic Operators

The PHP arithmetic operators are used with numeric values to perform common arithmetical operations, such as addition, subtraction, multiplication etc.

Operator 	Name 	Example 	Result 	Show it
== 	Equal 	$x == $y 	Returns true if $x is equal to $y 	
=== 	Identical 	$x === $y 	Returns true if $x is equal to $y, and they are of the same type 	
!= 	Not equal 	$x != $y 	Returns true if $x is not equal to $y 	
<> 	Not equal 	$x <> $y 	Returns true if $x is not equal to $y 	
!== 	Not identical 	$x !== $y 	Returns true if $x is not equal to $y, or they are not of the same type 	
> 	Greater than 	$x > $y 	Returns true if $x is greater than $y 	
< 	Less than 	$x < $y 	Returns true if $x is less than $y 	
>= 	Greater than or equal to 	$x >= $y 	Returns true if $x is greater than or equal to $y 	
<= 	Less than or equal to 	$x <= $y 	Returns true if $x is less than or equal to $y


PHP Assignment Operators

The PHP assignment operators are used with numeric values to write a value to a variable.

The basic assignment operator in PHP is "=". It means that the left operand gets set to the value of the assignment expression on the right.

Assignment 	Same as... 	Description 	Show it
x = y 	x = y 	The left operand gets set to the value of the expression on the right 	
x += y 	x = x + y 	Addition 	
x -= y 	x = x - y 	Subtraction 	
x *= y 	x = x * y 	Multiplication 	
x /= y 	x = x / y 	Division 	
x %= y 	x = x % y 	Modulus

## PHP Arithmetic Operators

The PHP arithmetic operators are used with numeric values to perform common arithmetical operations, such as addition, subtraction, multiplication etc.
Operator 	Name 	Example 	Result 	Show it
+ 	Addition 	$x + $y 	Sum of $x and $y 	
- 	Subtraction 	$x - $y 	Difference of $x and $y 	
* 	Multiplication 	$x * $y 	Product of $x and $y 	
/ 	Division 	$x / $y 	Quotient of $x and $y 	
% 	Modulus 	$x % $y 	Remainder of $x divided by $y 	
** 	Exponentiation 	$x ** $y 	Result of raising $x to the $y'th power (Introduced in PHP 5.6)



## PHP Increment / Decrement Operators

The PHP increment operators are used to increment a variable's value.

The PHP decrement operators are used to decrement a variable's value.
Operator 	Name 	Description 	Show it
++$x 	Pre-increment 	Increments $x by one, then returns $x 	
$x++ 	Post-increment 	Returns $x, then increments $x by one 	
--$x 	Pre-decrement 	Decrements $x by one, then returns $x 	
$x--


## PHP Logical Operators

The PHP logical operators are used to combine conditional statements.
Operator 	Name 	Example 	Result 	Show it
and 	And 	$x and $y 	True if both $x and $y are true 	
or 	Or 	$x or $y 	True if either $x or $y is true 	
xor 	Xor 	$x xor $y 	True if either $x or $y is true, but not both 	
&& 	And 	$x && $y 	True if both $x and $y are true 	
|| 	Or 	$x || $y 	True if either $x or $y is true 	
! 	Not 	!$x 	True if $x is not true 	
PHP String Operators

PHP has two operators that are specially designed for strings.
Operator 	Name 	Example 	Result 	Show it
. 	Concatenation 	$txt1 . $txt2 	Concatenation of $txt1 and $txt2 	
.= 	Concatenation assignment 	$txt1 .= $txt2 	Appends $txt2 to $txt1 	
PHP Array Operators

## The PHP array operators are used to compare arrays.

Operator 	Name 	Example 	Result 	Show it
+ 	Union 	$x + $y 	Union of $x and $y 	
== 	Equality 	$x == $y 	Returns true if $x and $y have the same key/value pairs 	
=== 	Identity 	$x === $y 	Returns true if $x and $y have the same key/value pairs in the same order and of the same types 	
!= 	Inequality 	$x != $y 	Returns true if $x is not equal to $y 	
<> 	Inequality 	$x <> $y 	Returns true if $x is not equal to $y 	
!== 	Non-identity 	$x !== $y 	Returns true if $x is not identical to $y 	


## Interactive from command line

```bash
root@holmes:/sadmin/www/dat/nano/rch$ date
Tue Apr  2 11:18:07 EDT 2019
root@holmes:/sadmin/www/dat/nano/rch$ php -a
Interactive shell

php > echo date("d") ."\n" ;
02
php > echo date("m") ."\n" ;
04
php > echo date("Y") ."\n" ;
2019
php > $D = date("Y-m-d");
php > echo "$D\n"; 
2019-04-02
php > ^D
```


// Prints the day
echo date("l") . "<br>";    | Saturday

// Prints the day, date, month, year, time, AM or PM
echo date("l jS \of F Y h:i:s A") . "<br>"; | Saturday 16th of February 2019 03:28:16 PM

// Prints September 1, 1955 was on a Friday
echo "Sep 1,1955 was on a ".date("l", mktime(0,0,0,9,1,1955)) ; |Sep 1,1955 was on a Thursday

// Use a constant in the format parameter
echo date(DATE_RFC822) . "<br>"; | Sat, 16 Feb 19 15:28:16 +0000

// prints something like: 1975-10-03T00:00:00+00:00
echo date(DATE_ATOM,mktime(0,0,0,10,3,1975)); | 1975-10-03T00:00:00+00:00 
```



### date(format, timestamp)

Parameter 	Description
format 	Required. Specifies the format of the outputted date string. The following characters can be used:

    d - The day of the month (from 01 to 31)
    D - A textual representation of a day (three letters)
    j - The day of the month without leading zeros (1 to 31)
    l (lowercase 'L') - A full textual representation of a day
    N - The ISO-8601 numeric representation of a day (1 for Monday, 7 for Sunday)
    S - The English ordinal suffix for the day of the month (2 characters st, nd, rd or th. Works well with j)
    w - A numeric representation of the day (0 for Sunday, 6 for Saturday)
    z - The day of the year (from 0 through 365)
    W - The ISO-8601 week number of year (weeks starting on Monday)
    F - A full textual representation of a month (January through December)
    m - A numeric representation of a month (from 01 to 12)
    M - A short textual representation of a month (three letters)
    n - A numeric representation of a month, without leading zeros (1 to 12)
    t - The number of days in the given month
    L - Whether it's a leap year (1 if it is a leap year, 0 otherwise)
    o - The ISO-8601 year number
    Y - A four digit representation of a year
    y - A two digit representation of a year
    a - Lowercase am or pm
    A - Uppercase AM or PM
    B - Swatch Internet time (000 to 999)
    g - 12-hour format of an hour (1 to 12)
    G - 24-hour format of an hour (0 to 23)
    h - 12-hour format of an hour (01 to 12)
    H - 24-hour format of an hour (00 to 23)
    i - Minutes with leading zeros (00 to 59)
    s - Seconds, with leading zeros (00 to 59)
    u - Microseconds (added in PHP 5.2.2)
    e - The timezone identifier (Examples: UTC, GMT, Atlantic/Azores)
    I (capital i) - Whether the date is in daylights savings time (1 if Daylight Savings Time, 0 otherwise)
    O - Difference to Greenwich time (GMT) in hours (Example: +0100)
    P - Difference to Greenwich time (GMT) in hours:minutes (added in PHP 5.1.3)
    T - Timezone abbreviations (Examples: EST, MDT)
    Z - Timezone offset in seconds. The offset for timezones west of UTC is negative (-43200 to 50400)
    c - The ISO-8601 date (e.g. 2013-05-05T16:34:42+00:00)
    r - The RFC 2822 formatted date (e.g. Fri, 12 Apr 2013 12:01:05 +0200)
    U - The seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)

and the following predefined constants can also be used (available since PHP 5.1.0):

    DATE_ATOM - Atom (example: 2013-04-12T15:52:01+00:00)
    DATE_COOKIE - HTTP Cookies (example: Friday, 12-Apr-13 15:52:01 UTC)
    DATE_ISO8601 - ISO-8601 (example: 2013-04-12T15:52:01+0000)
    DATE_RFC822 - RFC 822 (example: Fri, 12 Apr 13 15:52:01 +0000)
    DATE_RFC850 - RFC 850 (example: Friday, 12-Apr-13 15:52:01 UTC)
    DATE_RFC1036 - RFC 1036 (example: Fri, 12 Apr 13 15:52:01 +0000)
    DATE_RFC1123 - RFC 1123 (example: Fri, 12 Apr 2013 15:52:01 +0000)
    DATE_RFC2822 - RFC 2822 (Fri, 12 Apr 2013 15:52:01 +0000)
    DATE_RFC3339 - Same as DATE_ATOM (since PHP 5.1.3)
    DATE_RSS - RSS (Fri, 12 Aug 2013 15:52:01 +0000)
    DATE_W3C - World Wide Web Consortium (example: 2013-04-12T15:52:01+00:00)

timestamp 	Optional. Specifies an integer Unix timestamp. Default is the current local time (time())

Date Examples

```bash
echo "Today is " . date("Y-m-d") . "<br>";
echo "Today is " . date("l");

Today is 2019-04-01
Today is Monday 


```


## Get a Simple Time

Here are some characters that are commonly used for times:

    H - 24-hour format of an hour (00 to 23)
    h - 12-hour format of an hour with leading zeros (01 to 12)
    i - Minutes with leading zeros (00 to 59)
    s - Seconds with leading zeros (00 to 59)
    a - Lowercase Ante meridiem and Post meridiem (am or pm)

The example below outputs the current time in the specified format:
Example
<?php
echo "The time is " . date("h:i:sa");
?> 

$d=strtotime("tomorrow");
echo date("Y-m-d h:i:sa", $d) . "<br>";

$d=strtotime("next Saturday");
echo date("Y-m-d h:i:sa", $d) . "<br>";

$d=strtotime("+3 Months");
echo date("Y-m-d h:i:sa", $d) . "<br>";

2019-04-02 12:00:00am
2019-04-06 12:00:00am
2019-07-01 04:59:04pm

## Epoch Time

```php 
$timestamp = time(); 
echo($timestamp); 
echo "\n"; 
echo(date("F d, Y h:i:s A", $timestamp)); 
```
**Output:**

1512486297
December 05, 2017 03:04:57 PM

The mktime() function is used to create the timestamp for a specific date and time.
If no date and time is provided, the timestamp for the current date and time is returned.

mktime(hour, minute, second, month, day, year)

```php 
echo mktime(23, 21, 50, 11, 25, 2017); 
```
**Output:**

1511652110





## Accept Date solution

```bash
echo "\n<form>";
echo "\n<label>Date: </label>";
echo "<input type='date' name='sdate' value='$YESTERDAY2' size='11' min='2016-01-01' max='2018-12-31'/>";
echo "\n<input type='date'/><br>";
echo "\n<input type='submit'/>";
echo "\n</form>";
```
