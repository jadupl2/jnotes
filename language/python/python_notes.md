# Python Notes

- [Python Notes](#python-notes)
  - [OS Module](#os-module)
    - [How-to start a web server with python](#how-to-start-a-web-server-with-python)
  - [Delete last line of a file](#delete-last-line-of-a-file)



## OS Module
| Module                    | Description |
| :------                   | :-------------------|
| os.path.exists(WFILE)     | Test if file exist |
| os.remove(WFILE)          | Delete a file |





### How-to start a web server with python
```bash
jacques@holmes:~$ python3 -m http.server
Serving HTTP on 0.0.0.0 port 8000 ...
192.168.1.8 - - [21/Feb/2019 10:01:31] "GET / HTTP/1.1" 200 -
192.168.1.8 - - [21/Feb/2019 10:01:31] code 404, message File not found
192.168.1.8 - - [21/Feb/2019 10:01:31] "GET /favicon.ico HTTP/1.1" 404 -
192.168.1.8 - - [21/Feb/2019 10:01:39] "GET /.bash_history HTTP/1.1" 200 -
192.168.1.8 - - [21/Feb/2019 10:01:46] "GET /.config/ HTTP/1.1" 200 -
192.168.1.8 - - [21/Feb/2019 10:01:48] "GET /.config/Code/ HTTP/1.1" 200 -
192.168.1.8 - - [21/Feb/2019 10:01:53] "GET /.config/Code/User/ HTTP/1.1" 200 -
192.168.1.8 - - [21/Feb/2019 10:01:57] "GET /.config/Code/User/settings.json HTTP/1.1" 200 -
192.168.1.8 - - [21/Feb/2019 10:02:08] "GET /.config/Code/User/settings.json HTTP/1.1" 200 -
^C
Keyboard interrupt received, exiting.
jacques@holmes:~$
```

To open a text file, use:
fh = open("hello.txt", "r")

To read a text file, use:
fh = open("hello.txt","r")
print fh.read()

To read one line at a time, use:
fh = open("hello".txt", "r")
print fh.readline()

To read a list of lines use:
fh = open("hello.txt.", "r")
print fh.readlines()

To write to a file, use:
fh = open("hello.txt","w")
write("Hello World")
fh.close()

To write to a file, use:
fh = open("hello.txt", "w")
lines_of_text = ["a line of text", "another line of text", "a third line"]
fh.writelines(lines_of_text)
fh.close()

To append to file, use:
fh = open("Hello.txt", "a")
write("Hello World again")
fh.close()

To close a file, use
fh = open("hello.txt", "r")
print fh.read()
fh.close()

## Delete last line of a file

```python3
#!/usr/bin/env python3
import os,sys
filename="/sadmin/dat/rch/holmes_sadm_template.rch"
with open(filename) as xrch:
    lastLine = (list(xrch)[-1])
    print ("Last Line : ",lastLine)

rch_code = lastLine.split(' ')[8].strip()
print ("RCH Code  : ",rch_code)
if rch_code == "2" :    
    print ("Code 2 detected")

    rch_file = open(filename,'r')
    lines = rch_file.readlines() 
    print ('Before : ',lines)
    rch_file.close()
    
    del lines[-1] 
    
    rch_file = open(filename,'w')
    print ('After  : ',lines) 
    rch_file.writelines(lines)
    rch_file.close()

print ("Exit")
```
