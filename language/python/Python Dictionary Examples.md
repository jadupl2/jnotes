# Python Dictionary Examples

Create, Update and Delete Elements  
In this tutorial, we’ll understand the basics of python dictionaries with examples.  
Dictionary in python consists of *keys* and their *values*.   

---

### Create a Python Dictionary

Here is an example of how we can create a dictionary in Python :

```python
>>> myDict = {"A":"Apple", "B":"Boy", "C":"Cat"}
```

In the above example:

* A dictionary is created.
* This dictionary contains three elements.
* Each element constitutes of a key value pair.
* This dictionary can be accessed using the variable myDict.

---

### Access Dictionary Elements

Once a dictionary is created, you can access it using the variable to which it is assigned during creation. For example, in our case, the variable myDict can be used to access the dictionary elements.

Here is how this can be done :

```python
>>> myDict["A"]
'Apple'
>>> myDict["B"]
'Boy'
>>> myDict["C"]
'Cat'
```

So you can see that using the variable myDict and Key as index, the value of corresponding key can be accessed. For those who have C/C++ background, its more like accessing the value kept at a particular index in an array.

If you just type the name of the variable myDict, all the key value pairs in the dictionary will be printed.

```python
>>> myDict
{'A': 'Apple', 'C': 'Cat', 'B': 'Boy'}
```

There is one thing that you need to keep in your mind. Only dictionary keys can be used as indexes. This means that myDict[“A”] would produce ‘Apple’ in output but myDict[“Apple”] cannot produce ‘A’ in the output.

```python
>>> myDict["Apple"]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
KeyError: 'Apple'
```

So we see that the python compiler complained about ‘Apple’ being used as index.

---

### Update Dictionary Elements

Just the way dictionary values are accessed using keys, the values can also be modified using the dictionary keys. Here is an example to modify python dictionary element:

```python
>>> myDict["A"] = "Application"
>>> myDict["A"]
'Application'
>>> myDict
{'A': 'Application', 'C': 'Cat', 'B': 'Boy'}
```

You can see that in the example shown above, the value of key ‘A’ was changed from ‘Apple’ to ‘Application’ easily.  This way we can easily conclude that there could not be two keys with same name in a dictionary.

---

### Delete Dictionary Elements

Individual elements can be deleted easily from a dictionary. Here is an example to remove an element from dictionary.

```python
>>> myDict
{'A': 'Application', 'C': 'Cat', 'B': 'Boy'}
>>> del myDict["A"]
>>> myDict
{'C': 'Cat', 'B': 'Boy'}
```

So you can see that by using ‘del’ an element can easily be deleted from the dictionary.
If you want to delete complete dictionary ie all the elements in the dictionary then it can be done using the clear() function. Here is an example :

```python
>>> myDict
{'C': 'Cat', 'B': 'Boy'}
>>> myDict.clear()
>>> myDict
{}
```

So you see that all the elements were deleted making the dictionary empty.

### Characteristics of Python Dictionaries

After discussing all the actions that can be done on dictionaries, lets now understand a couple of important characteristics of dictionaries in Python.

### Dictionaries are Unordered

Just have a look at the earlier examples that we have used in this article, you will observe that the dictionary elements (key-value pairs) are not in ordered form. Let’s focus on this aspect once again :

```python
>>> myDict = {"A":"Apple", "B":"Boy", "C":"Cat"}
>>> myDict
{'A': 'Apple', 'C': 'Cat', 'B': 'Boy'}
```

You can observe that the order of elements while the dictionary was being created is different from the order in which they are actually stored and displayed.
Even if you try to add other elements to python dictionary:

```python
>>> myDict["D"] = "Dog"
>>> myDict
{'A': 'Apple', 'C': 'Cat', 'B': 'Boy', 'D': 'Dog'}
>>> myDict["E"] = "Elephant"
>>> myDict
{'A': 'Apple', 'C': 'Cat', 'B': 'Boy', 'E': 'Elephant', 'D': 'Dog'}
```

You’ll observe that it’s not necessary that elements will be stored in the same order in which they were created.

---

### Dictionary Keys are Case Sensitive

Yes, this is true. Same key name but with different case are treated as different keys in python dictionaries.   
Here is an example :

```python
>>> myDict["F"] = "Fan"
>>> myDict["f"] = "freeze"
>>> myDict
{'A': 'Apple', 'C': 'Cat', 'B': 'Boy', 'E': 'Elephant', 'D': 'Dog', 'F': 'Fan', 'f': 'freeze'}
```

In the example above, we tried to create two elements with same key name but different case and you can observe that to separate elements corresponding to same key were created.

---

Tags:
  dictionnary, python
