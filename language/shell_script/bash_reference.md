# Bash Reference

<!--
Jacques Duplessis
2018_11_20 v1.0 Initial Version
2019_03_21 v1.1 Begin Markdown formatting 
-->

- [Bash Reference](#bash-reference)
  - [Flow controls (Operators, Loop)](#flow-controls-operators-loop)
    - [Strings](#strings)
    - [Files](#files)
    - [Bash parameter expansion syntaxes](#bash-parameter-expansion-syntaxes)
    - [Numbers](#numbers)
    - [If statement](#if-statement)
    - [For loop](#for-loop)
    - [Case statement](#case-statement)
    - [Select Statement](#select-statement)
    - [While/Until Loop](#whileuntil-loop)
  - [Keyboard shortcuts](#keyboard-shortcuts)
  - [Variables](#variables)
  - [Functions](#functions)
  - [Command-line processing cycle](#command-line-processing-cycle)
  - [Input/Output Redirectors](#inputoutput-redirectors)
  - [Read File](#read-file)
  - [Ask Confirmation (y/n)](#ask-confirmation-yn)
  - [Process Handling](#process-handling)
  - [TIPS & TRICKS](#tips--tricks)
  - [Debugging Shell Scripts](#debugging-shell-scripts)
    - [LOOP](#loop)
    - [TEST](#test)
    - [In place string replace](#in-place-string-replace)
  - [Using variables outside a while loop](#using-variables-outside-a-while-loop)

## Flow controls (Operators, Loop)

```bash
statement1 && statement2  # and operator
statement1 || statement2  # or operator

-a                        # and operator inside a test conditional expression
-o                        # or operator inside a test conditional expression
```

### Strings
```bash
str1 = str2               # str1 matches str2
str1 != str2              # str1 does not match str2
str1 < str2               # str1 is less than str2 (alphabetically)
str1 > str2               # str1 is greater than str2 (alphabetically)
-n str1                   # str1 is not null (has length greater than 0)
-z str1                   # str1 is null (has length 0)
```

### Files
```bash
[ ! -f "$in" ] && { echo "$0 - File $in not found."; exit 1; }
-a file                   # file exists
-d file                   # file exists and is a directory
-e file                   # file exists; same -a
-f file                   # file exists and is a regular file (i.e., not a directory or other special type of file)
-r file                   # you have read permission
-s file                   # file exists and is not empty
-w file                   # your have write permission
-x file                   # you have execute permission on file, or directory search permission if it is a directory
-N file                   # file was modified since it was last read
-O file                   # you own file
-G file                   # file's group ID matches yours (or one of yours, if you are in multiple groups)
file1 -nt file2           # file1 is newer than file2
file1 -ot file2           # file1 is older than file2
```


### Bash parameter expansion syntaxes

```bash
Parameter Expansion         Description
${variable:-value}          If the variable is unset or undefined then expand the value.
${variable:=value}          If the variable is unset or undefined then set the value to the variable.
${variable:+value}          If the variable is set or defined then expand the value.
${variable:start:length}    Substring will retrieve from the start position to length position of the variable.
${variable:start} 	        Substring will retrieve from start position to the remaining part of the variable.
${#variable} 	            Count the length of the variable.
${variable/pattern/string} 	Replace the part of the variable with string where the pattern match for the first time.
${variable//pattern/string} Replace all occurrences in the variable with string where all pattern matches.
${variable/#pattern/string} If the pattern exists at the beginning of the variable, then replace the occurrence with string.
${variable/%pattern/string} If the pattern exists at the end of the variable, then replace the occurrence with string.
${variable#pattern}         Remove the shortest match from the beginning of the variable where the pattern matches.
${variable##pattern}        Remove the longest match from the beginning of the variable where the pattern matches.
${variable%pattern}         Remove the shortest match from the end of the variable where the pattern matches.
${variable%%pattern}        Remove the longest match from the end of the variable where the pattern matches.
```


### Numbers

```bash
-lt                       # less than
-le                       # less than or equal
-eq                       # equal
-ge                       # greater than or equal
-gt                       # greater than
-ne                       # not equal
```

### If statement

```bash
if condition
then
  statements
[elif condition
  then statements...]
[else
  statements]
fi
```

### For loop

```bash
for x in {1..10}
do
  statements
done

#/bin/bash   
TESTSTR="abc,def,ghij"
for i in $(echo $TESTSTR | tr ',' '\n')
do
echo $i
done
abc
def
ghij

for name [in list]  [for n in $(cat lists.txt )]
do
  statements that can use $name
done

for (( initialisation ; ending condition ; update ))
do
  statements...
done
```

### Case statement

```bash
    case "$atype" in                                                    # Depending on Alert Type
      e|E) ws="SADM ERROR: ${asubject}"                                 # Construct Mess. Subject
           ;;
      w|W) ws="SADM WARNING: ${asubject}"                               # Build Warning Subject
           ;;
      i|I) ws="SADM INFO: ${asubject}"                                  # Build Info Mess Subject
           ;;
      S|S) ws="SADM SCRIPT: ${asubject}"                                # Build Script Msg Subject
           ;;
      e|E) ws="SADM ERROR: ${asubject}"                                 # Construct Mess. Subject
           ;;
        *) ws="Invalid Alert Type ($atype): ${asubject}"                # Invalid Alert type Message
           ;;
    esac
```

### Select Statement

```bash
select name [in list]
do
  statements that can use $name
done
```

### While/Until Loop

```bash
while condition; do
  statements
done

until condition; do
  statements
done
```


## Keyboard shortcuts

| Key     | Description            | 
|:------: | :---------                 |
| CTRL+A  | move to beginning of line | 
| CTRL+B  | moves backward one character  | 
| CTRL+C  | halts the current command  | 
| CTRL+D  | deletes one character backward or logs out of current session, similar to exit  | 
| CTRL+E  | moves to end of line  | 
| CTRL+F  | moves forward one character   | 
| CTRL+G  | aborts the current editing command and ring the terminal bell  | 
| CTRL+J  | same as RETURN  | 
| CTRL+K  | deletes (kill) forward to end of line  | 
| CTRL+L  | clears screen and redisplay the line  | 
| CTRL+M  | same as RETURN  | 
| CTRL+N  | next line in command history  | 
| CTRL+O  | same as RETURN, then displays next line in history file  | 
| CTRL+P  | previous line in command history  | 
| CTRL+R  | searches backward  | 
| CTRL+S  | searches forward  | 
| CTRL+T  | transposes two characters  | 
| CTRL+U  | kills backward from point to the beginning of line  | 
| CTRL+V  | makes the next character typed verbatim  | 
| CTRL+W  | kills the word behind the cursor  | 
| CTRL+X  | lists the possible filename completions of the current word  | 
| CTRL+Y  | retrieves (yank) last item killed  |
| CTRL+Z  | stops the current command, resume with fg in the foreground or bg in the background| 
| ALT+B   |  moves backward one word  | 
| ALT+D   |  deletes next word  | 
| ALT+F   | moves forward one word  | 
| DELETE  | deletes one character backward  | 
| !!      |  repeats the last command  | 
| exit    | logs out of current session  | 


## Variables

```bash
$ echo $SHELL
/bin/bash
$ echo $BASH_VERSION
4.2.46(2)-release

varname=value                # defines a variable
varname=value command        # defines a variable to be in the environment of a particular subprocess
echo $varname                # checks a variable's value
echo $$                      # prints process ID of the current shell
echo $!                      # prints process ID of the most recently invoked background job
echo $?                      # displays the exit status of the last command
export VARNAME=value         # defines an environment variable (will be available in subprocesses)

array[0]=valA                # how to define an array
array[1]=valB
array[2]=valC
array=([2]=valC [0]=valA [1]=valB)  # another way
array=(valA valB valC)              # and another

${array[i]}                  # displays array's value for this index. If no index is supplied, array element 0 is assumed
${#array[i]}                 # to find out the length of any element in the array
${#array[@]}                 # to find out how many values there are in the array

declare -a                   # the variables are treaded as arrays
declare -f                   # uses function names only
declare -F                   # displays function names without definitions
declare -i                   # the variables are treaded as integers
declare -r                   # makes the variables read-only
declare -x                   # marks the variables for export via the environment

${varname:-word}             # if varname exists and isn't null, return its value; otherwise return word
${varname:=word}             # if varname exists and isn't null, return its value; otherwise set it word and then return its value
${varname:?message}          # if varname exists and isn't null, return its value; otherwise print varname, followed by message and abort the current command or script
${varname:+word}             # if varname exists and isn't null, return word; otherwise return null
${varname:offset:length}     # performs substring expansion. It returns the substring of $varname starting at offset and up to length characters

${variable#pattern}          # if the pattern matches the beginning of the variable's value, delete the shortest part that matches and return the rest
${variable##pattern}         # if the pattern matches the beginning of the variable's value, delete the longest part that matches and return the rest
${variable%pattern}          # if the pattern matches the end of the variable's value, delete the shortest part that matches and return the rest
${variable%%pattern}         # if the pattern matches the end of the variable's value, delete the longest part that matches and return the rest
${variable/pattern/string}   # the longest match to pattern in variable is replaced by string. Only the first match is replaced
${variable//pattern/string}  # the longest match to pattern in variable is replaced by string. All matches are replaced

${#varname}                  # returns the length of the value of the variable as a character string

*(patternlist)               # matches zero or more occurrences of the given patterns
+(patternlist)               # matches one or more occurrences of the given patterns
?(patternlist)               # matches zero or one occurrence of the given patterns
@(patternlist)               # matches exactly one of the given patterns
!(patternlist)               # matches anything except one of the given patterns

$(UNIX command)              # command substitution: runs the command and returns standard output
```


## Functions

```bash
# The function refers to passed arguments by position (as if they were positional parameters), that is, $1, $2, and so forth.
# $@ is equal to "$1" "$2"... "$N", where N is the number of positional parameters. $# holds the number of positional parameters.

function functname() {
  shell commands
}

unset -f functname  # deletes a function definition
declare -f          # displays all defined functions in your login session
```


## Command-line processing cycle


The default order for command lookup is functions, followed by built-ins, with scripts and executables last.  
There are three built-ins that you can use to override this order: `command`, `builtin` and `enable`.  

```bash
command  # removes alias and function lookup. Only built-ins and commands found in the search path are executed
builtin  # looks up only built-in commands, ignoring functions and commands found in PATH
enable   # enables and disables shell built-ins
eval     # takes arguments and run them through the command-line processing steps all over again
```

## Input/Output Redirectors

```bash
cmd1|cmd2  # pipe; takes standard output of cmd1 as standard input to cmd2
< file     # takes standard input from file
> file     # directs standard output to file
>> file    # directs standard output to file; append to file if it already exists
>|file     # forces standard output to file even if noclobber is set
n>|file    # forces output to file from file descriptor n even if noclobber is set
<> file    # uses file as both standard input and standard output
n<>file    # uses file as both input and output for file descriptor n
n>file     # directs file descriptor n to file
n<file     # takes file descriptor n from file
n>>file    # directs file description n to file; append to file if it already exists
n>&        # duplicates standard output to file descriptor n
n<&        # duplicates standard input from file descriptor n
n>&m       # file descriptor n is made to be a copy of the output file descriptor
n<&m       # file descriptor n is made to be a copy of the input file descriptor
&>file     # directs standard output and standard error to file
<&-        # closes the standard input
>&-        # closes the standard output
n>&-       # closes the ouput from file descriptor n
n<&-       # closes the input from file descripor n
```

## Read File 

```bash
set -e
in="${1:-input.txt}"
 
[ ! -f "$in" ] && { echo "$0 - File $in not found."; exit 1; }
 
while IFS= read -r file
    do
    echo "Working on $file ..."
    done < "${in}"
```

## Ask Confirmation (y/n)

```bash
while :
    do
    printf "$wmess  ${RIGHT}${RIGHT}"                         # Write mess rcv + [ Y/N ] ?
    read answer                                                     # Read User answer
    case "$answer" in                                               # Test Answer
        Y|y ) wreturn=1                                              # Yes = Return Value of 1
              break                                                  # Break of the loop
              ;; 
        n|N ) wreturn=0                                              # No = Return Value of 0
              break                                                  # Break of the loop
              ;;
          * ) ;;                                                     # Other stay in the loop
    esac
    done
return $wreturn
```


## Process Handling

```bash
# To suspend a job, type CTRL+Z while it is running. You can also suspend a job with CTRL+Y.
# This is slightly different from CTRL+Z in that the process is only stopped when it attempts to read input from terminal.
# Of course, to interrupt a job, type CTRL+C.

myCommand &  # runs job in the background and prompts back the shell

jobs         # lists all jobs (use with -l to see associated PID)

fg           # brings a background job into the foreground
fg %+        # brings most recently invoked background job
fg %-        # brings second most recently invoked background job
fg %N        # brings job number N
fg %string   # brings job whose command begins with string
fg %?string  # brings job whose command contains string

kill -l      # returns a list of all signals on the system, by name and number
kill PID     # terminates process with specified PID

ps           # prints a line of information about the current running login shell and any processes running under it
ps -a        # selects all processes with a tty except session leaders

trap cmd sig1 sig2  # executes a command when a signal is received by the script
trap "" sig1 sig2   # ignores that signals
trap - sig1 sig2    # resets the action taken when the signal is received to the default

disown <PID|JID>    # removes the process from the list of jobs

wait                # waits until all background jobs have finished
```


## TIPS & TRICKS

```bash
# set an alias
cd; nano .bash_profile
> alias gentlenode='ssh admin@gentlenode.com -p 3404'  # add your alias in .bash_profile

# to quickly go to a specific directory
cd; nano .bashrc
> shopt -s cdable_vars
> export websites="/Users/mac/Documents/websites"

source .bashrc
cd $websites
```


## Debugging Shell Scripts

```bash
bash -n scriptname  # don't run commands; check for syntax errors only
set -o noexec       # alternative (set option in script)

bash -v scriptname  # echo commands before running them
set -o verbose      # alternative (set option in script)

bash -x scriptname  # echo commands after command-line processing
set -o xtrace       # alternative (set option in script)

trap 'echo $varname' EXIT  # useful when you want to print out the values of variables at the point that your script exits

function errtrap {
  es=$?
  echo "ERROR line $1: Command exited with status $es."
}

trap 'errtrap $LINENO' ERR  # is run whenever a command in the surrounding script or function exits with non-zero status 

function dbgtrap {
  echo "badvar is $badvar"
}

trap dbgtrap DEBUG  # causes the trap code to be executed before every statement in a function or script
# ...section of code in which the problem occurs...
trap - DEBUG  # turn off the DEBUG trap

function returntrap {
  echo "A return occurred"
}

trap returntrap RETURN  # is executed each time a shell function or a script executed with the . or source commands finishes executing
```


OneSheet Shell Scripts

### LOOP
```bash
for  file in $(ls); do ... done
for (( c=1; c<=5; c++ ))
do  
   echo "Welcome $c times"
done
for i in {1..5}
do
   echo "Welcome $i times"
done
echo "Bash version 4 "
for i in {0..10..2}
  do 
     echo "Welcome $i times"
 done
for i in $(seq 1 2 20)
do
   echo "Welcome $i times"
done
for (( ; ; ))
do
   echo "infinite loops [ hit CTRL+C to stop]"
done
for I in 1 2 3 4 5
do
  statements1      #Executed for all values of ''I'', up to a disaster-condition if any.
  statements2
  if (disaster-condition)
  then
	break       	   #Abandon the loop.
  fi
  statements3          #While good and, no disaster-condition.
done
for file in /etc/*
do
	if [ "${file}" == "/etc/resolv.conf" ]
	then
		countNameservers=$(grep -c nameserver /etc/resolv.conf)
		echo "Total  ${countNameservers} nameservers defined in ${file}"
		break
	fi
done
```

### TEST
```bash
if [ "${myvar}" == "foo" ]; then

### REMOVE HEADING & TRAILING SPACES
# echo "    Jacques    " | awk '{$1=$1;print}'
Jacques
# 

# L
```

### In place string replace
```bash
# Disable EPEL Repository from the YUM update
write_log "Disable the EPEL Repository from normal yum update"
write_log "perl -p -i -e 's/enabled=1/enabled=0/g'  /etc/yum.repos.d/epel.repo"
perl -p -i -e 's/enabled=1/enabled=0/g'  /etc/yum.repos.d/epel.repo
EC=$? ; write_log "Return code after changing epel.repo is $EC"
```



## Using variables outside a while loop

Bash runs every loop (while or for) in a subshell. 
The values for a variable defined inside such a loop / subshell are not available outside.


```bash
#!/bin/bash
t=0
while read line; do
  t=$(( t + 1 ))
done < /etc/passwd
echo $t
```

Returns 0 for $t but inside the loop the value is right.
To use a variable outside a loop, just extend the subshell.

```bash
#!/bin/bash
t=0
cat /etc/passwd | { while read line; do
  t=$(( t + 1 ))
done;
echo $t
}
```

